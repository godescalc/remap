module PRIMS
 use MISC
 use LIST
 implicit none

 real(8),parameter :: cutoff=0.001
 real(8),parameter :: PI=2.*acos(0.)
 integer(4),parameter :: shtlist(-2:2)=(/5,4,1,3,6/)
 integer(4) :: expl_6D(6,3),expl_5D(5,3)
 ! shell-type list. 0 is s.
 ! +1 is p (3 bfs), -1 is sp (4 bfs)
 ! +2 is d (6 bfs), -2 is sd (5 bfs)

 type PRIM
  real(8) :: coeff
  real(8) :: xyz(3)
  real(8) :: expnt
  integer(4) :: pow(3)
 end type PRIM

 type ALLOCVEC
  real(8),allocatable :: vec(:,:)
  real(8),allocatable :: dvec(:)
 end type ALLOCVEC

 type BASIS
 end type BASIS

 type WAVEFN
  character :: nomen*20
  integer(4) :: nat
  integer(4),allocatable :: zat(:)
  real(8),allocatable :: xyz(:,:)
  
  integer(4) :: nel ! number of electrons 
  integer(4) :: nA ! number of alpha electrons 
  integer(4) :: nB ! number of beta electrons 
  real(8) :: nelint ! integration of density
  integer(4) :: nbas
  integer(4) :: nprim
  real(8),allocatable :: basprim(:,:) ! data of primitives in basis function
! primitives go in here...
  type (PRIM),allocatable :: p(:)
! mos
  integer(4) :: nmos
  real(8),allocatable :: cb(:,:) ! coeff. of BASIS
  real(8),allocatable :: cp(:,:) ! coeff. of PRIMS
  real(8),allocatable :: dmat(:,:) ! prim x prim dmat
  real(8),allocatable :: dpp (:,:,:) ! integral of prim x prim in direction u
                                     ! dpp(i,j,k=Dprimprod(wfn%p(i),wfn%p(j),k)
  real(8),allocatable :: pp_z(:,:,:) ! product of prim x prim in direction z for point k
  real(8),allocatable :: pp_yz(:,:,:,:) ! product of p x p in y for point (j,k)
  real(8),allocatable :: basd(:,:,:) ! density matrix of basis function
  integer(4) :: n_inv ! number of points (rounded up to power of 2) in n-space mapping (a[xy]z,fn)
  real(8),allocatable :: az(:),ayz(:,:),axyz(:,:,:) ! inverted 
!  real(8) :: arange(3,2)
  real(8),allocatable :: cbos(:,:,:)
  real(8),allocatable :: dens(:,:,:)
  real(8),allocatable :: mos(:,:,:,:)
!  real(8),allocatable :: fr(:,:,:) ! f-functions in real-space
  real(8),allocatable :: fn(:,:,:,:) ! f-functions in n-space

  type(ALLOCVEC) :: spec_u(3)
  integer(4) :: nspec(3)=-1
  real(8) :: minseg(3)
 end type WAVEFN

 contains

  subroutine N_lookup(wfn,ncoI,coO)
   implicit none
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: ncoI(3) ! coords in
   real(8),intent(out) :: coO(3) ! coords out
   real(8) :: nIp2(3),remndr,rout,d1,d2,ntr1,ntr2,intyz,intxyz,n_t1,n_t2
   integer :: i,j,k,l,n1,n2,nz,ny
!   call cpu_time(n_t1)
   nIp2=ncoI*wfn%n_inv
!   write(*,*) "Desired n-values: ",(ncoI(k),k=1,3)
!   write(*,*) "x 2^m           : ",(nIp2(k),k=1,3)
   ny=nint(nIp2(2)); nz=nint(nIp2(3))
! z first: 
   remndr=abs( nint(nIp2(3))-nIp2(3))
   if (remndr.lt.0.0001) then 
    coO(3)=wfn%az(nint(nIp2(3)))
   else
    n1=min( (wfn%n_inv-1),int(nIp2(3))) ; n2=n1+1
    d1=wfn%az(n1); d2=wfn%az(n2)
    call invert_nz(wfn,ncoI(3),d1,d2,rout)
    coO(3)=rout
   endif 
!   write(*,*) "Z value : ",rout
!   write(*,'(A6,2F8.4)') "Comp: ",ncoI(3),nint_z_FULL(wfn,rout)
! now y:
   n1=min( (wfn%n_inv-1),int(nIp2(2))) ; n2=n1+1
   d1=wfn%ayz(n1,nz); d2=wfn%ayz(n2,nz)
!   ntr1=nint_yz_FULL(wfn,d1,coO(3))
!   ntr2=nint_yz_FULL(wfn,d2,coO(3))
!   write(*,*) "targ: ",ncoI(2),"lim: ", ntr1,ntr2
!   if (n1.gt.0) then;do while (ntr1.gt.ncoI(2))
!    n1=n1-1;n2=n2-1
!    d1=wfn%ayz(n1,nz); d2=wfn%ayz(n2,nz)
!    ntr1=nint_yz_FULL(wfn,d1,coO(3))
!    ntr2=nint_yz_FULL(wfn,d2,coO(3))
!    write(*,*) "targ: ",ncoI(2),"lim: ", ntr1,ntr2
!   enddo;
!   elseif (n2.lt.wfn%n_inv) then; do while (ntr2.lt.ncoI(2))
!    n1=n1+1;n2=n2+1
!    d1=wfn%ayz(n1,nz); d2=wfn%ayz(n2,nz)
!    ntr1=nint_yz_FULL(wfn,d1,coO(3))
!    ntr2=nint_yz_FULL(wfn,d2,coO(3))
!    write(*,*) "targ: ",ncoI(2),"lim: ", ntr1,ntr2
!   enddo;endif
   intyz=nint_yz_FULL(wfn,100000._8,coO(3))
   call invert_nyz(wfn,coO(3),intyz*ncoI(2),d1,d2,rout) 
   coO(2)=rout
!   write(*,*) "Y value : ",rout
!   write(*,'(A6,2F8.4)') "Comp: ",ncoI(2),(nint_yz_FULL(wfn,coO(2),coO(3))/intyz) !nint_yz_FULL(wfn,10000._8,coO(3)))
! now x:
   n1=min( (wfn%n_inv-1),int(nIp2(1))) ; n2=n1+1
   d1=wfn%axyz(n1,ny,nz); d2=wfn%axyz(n2,ny,nz)
   intxyz=nint_xyz_FULL(wfn,100000._8,coO(2),coO(3))
!   ntr1=nint_xyz_FULL(wfn,d1,coO(2),coO(3))
!   ntr2=nint_xyz_FULL(wfn,d2,coO(2),coO(3))
!   write(*,*) "targ: ",ncoI(1),"lim: ", ntr1,ntr2
!   if (n1.gt.0) then;do while (ntr1.gt.ncoI(1))
!    n1=n1-1;n2=n2-1
!    d1=wfn%axyz(n1,ny,nz); d2=wfn%axyz(n2,ny,nz)
!    ntr1=nint_xyz_FULL(wfn,d1,coO(2),coO(3))
!    ntr2=nint_xyz_FULL(wfn,d2,coO(2),coO(3))
!    write(*,*) "targ: ",ncoI(1),"lim: ", ntr1,ntr2
!   enddo;
!   elseif (n2.lt.wfn%n_inv) then; do while (ntr2.lt.ncoI(1))
!    n1=n1+1;n2=n2+1
!    d1=wfn%axyz(n1,ny,nz); d2=wfn%axyz(n2,ny,nz)
!    ntr1=nint_xyz_FULL(wfn,d1,coO(2),coO(3))
!    ntr2=nint_xyz_FULL(wfn,d2,coO(2),coO(3))
!    write(*,*) "targ: ",ncoI(1),"lim: ", ntr1,ntr2
!   enddo;endif
   call invert_nxyz(wfn,coO(2),coO(3),intxyz*ncoI(1),d1,d2,rout) 
   coO(1)=rout
!   write(*,*) "X value : ",rout
!   write(*,'(A6,2F8.4)') "Comp: ",ncoI(1),(nint_xyz_FULL(wfn,coO(1),coO(2),coO(3))/intxyz)
  
   
!   call cpu_time(n_t1)
!   write(*,*) "Time: ",(n_t2-n_t1)

  
   

    
   return
  end subroutine N_lookup  


  integer function npow2(i2)
! rounds up to nearest power of two
   integer,intent(in) :: i2
   npow2=1
   do while (npow2.lt.i2)
    npow2=npow2*2
   enddo
   return
  end function npow2


  subroutine SPECGRID_U(wfn,tol,zmin,zmax,u)
   implicit none
   type(WAVEFN),intent(inout):: wfn
   real(8),intent(in) :: tol,zmin,zmax
   integer,intent(in) :: u
   type(NODE),pointer :: n0,n1,nmid,first,last
   real(8) :: z0,z1,n_t1,n_t2
   logical :: cont
   integer :: i
   call cpu_time(n_t1)
   allocate (n0); allocate(n1)
   n0%rval=zmin; n0%nval=nint_u_FULL(wfn,zmin,u)
   n1%rval=zmax; n1%nval=nint_u_FULL(wfn,zmax,u)
   n0%next=>n1
   first=>n0
   last =>n1
   cont=.true.
   do while ( cont)
    do while ( (abs(n0%nval-n1%nval).gt.tol)) 
!     write(*,*) "Difference : ",abs(n0%nval-n1%nval)
     allocate(nmid)
     n0%next=>nmid;nmid%next=>n1
     nmid%rval=0.5*(n0%rval+n1%rval)
     nmid%nval=nint_u_FULL(wfn,nmid%rval,u)
!     call printnode(n0)
!     call printnode(nmid)
!     call printnode(n1)
!     call list_list()
!     write(*,*) "Hit enter";read(*,*)
     n1=>nmid
    enddo
    cont=associated(n1%next)
    n1=>n1%next
    n0=>n0%next
   enddo
   call list2vec()
!   call list_list()
   call cpu_time(n_t2)
   write(*,*) " Mapped out direction ",u,". Time : ",(n_t2-n_t1)
   call minlist()
   call destroy()
   return
   contains  
    subroutine minlist()
     integer :: ii,ndd,nt
     real(8) :: ddlist(100,2),dd
     real(8),allocatable :: dlist(:)
     nt=wfn%nspec(u)
     allocate(dlist(nt))
     dlist(1)=wfn%spec_u(u)%vec(2,2)-wfn%spec_u(u)%vec(1,2)
     dlist(1)=dlist(1)/(wfn%spec_u(u)%vec(2,1)-wfn%spec_u(u)%vec(1,1))
     dlist(nt)=wfn%spec_u(u)%vec(nt,2)-wfn%spec_u(u)%vec(nt-1,2)
     dlist(nt)=dlist(nt)/(wfn%spec_u(u)%vec(nt,1)-wfn%spec_u(u)%vec(nt-1,1))
     do ii=2,nt-1
      dlist(ii)=wfn%spec_u(u)%vec(ii+1,2)-wfn%spec_u(u)%vec(ii-1,2)
      dlist(ii)=dlist(ii)/(wfn%spec_u(u)%vec(ii+1,1)-wfn%spec_u(u)%vec(ii-1,1))
     enddo

     ndd=1;ddlist(1,:)=wfn%spec_u(u)%vec(1,:)
     do ii=2,nt-1
      if ( dlist(ii).lt.(min(dlist(ii+1),dlist(ii-1)))) then
        ndd=ndd+1;ddlist(ndd,:)=wfn%spec_u(u)%vec(ii,:)
      endif
     enddo
     ndd=ndd+1;ddlist(ndd,:)=wfn%spec_u(u)%vec(nt,:)
     write(*,*) "Minlist : "
     do ii=1,ndd;write(*,*) ii,ddlist(ii,:);enddo
     wfn%minseg(u)=0.8
     do ii=2,ndd;wfn%minseg(u)=min(wfn%minseg(u),ddlist(ii,2)-ddlist(ii-1,2));enddo
     write(*,*) "Minseg : ",wfn%minseg(u)
     return
    end subroutine minlist
     
    subroutine list2vec()
     integer :: ind,nnd
     type(NODE),pointer :: curr
     nnd=countnodes()
     wfn%nspec(u)=nnd
     allocate(wfn%spec_u(u)%vec(nnd,2))
     allocate(wfn%spec_u(u)%dvec(nnd))
     curr=>first
     do ind=1,nnd
      wfn%spec_u(u)%vec(ind,1)=curr%rval
      wfn%spec_u(u)%vec(ind,2)=curr%nval
      if (associated(curr%next)) then; curr=>curr%next
      else;write(*,*) ind,"list over";endif
     enddo
     do ind=2,nnd-1
      wfn%spec_u(u)%dvec(ind)=0.5*(wfn%spec_u(u)%vec(ind+1,1)-wfn%spec_u(u)%vec(ind-1,1))
     enddo
     wfn%spec_u(u)%dvec(1)=(wfn%spec_u(u)%vec(2,1)-wfn%spec_u(u)%vec(1,1))
     wfn%spec_u(u)%dvec(nnd)=(wfn%spec_u(u)%vec(nnd,1)-wfn%spec_u(u)%vec(nnd-1,1))
     return
    end subroutine list2vec
    subroutine destroy()
    implicit none
    type(NODE), pointer :: curr,next
    integer :: ii
!    write(*,*) "Destroying list. "
    curr=>first
    ii=1
    do while (associated(curr%next))
     next=>curr%next
!     write(*,*) "Destroying item : ",ii
     deallocate(curr)
     curr=>next
     ii=ii+1
    enddo
!    write(*,*) "Destroying item : ",ii
    deallocate(curr)
    return
    end subroutine destroy
    subroutine printnode(nd)
     type(NODE) :: nd
     if (associated(nd%next)) then
      write(*,'(4F10.5)') nd%rval,nd%nval,nd%next%rval,nd%next%nval
     else
      write(*,'(2F10.5,A10)') nd%rval,nd%nval," termnl "
     endif
     return
    end subroutine printnode
    integer function countnodes()
     type(NODE),pointer:: current
     integer :: nnodes
     current => first
     nnodes=1
     do while(associated(current%next))
      current=>current%next
      nnodes=nnodes+1
     enddo
     countnodes=nnodes
     return
    end function countnodes 
    subroutine list_list()
     type(NODE),pointer:: current
     integer :: nnodes
     nnodes=1
     current => first
!     write(*,*) current%rval,current%nval
     do while(associated(current%next))
      write(*,*) nnodes,current%rval,current%nval !,nint_u_FULL(wfn,current%rval,3)
      current=>current%next
      nnodes=nnodes+1
     enddo
     write(*,*) nnodes,current%rval,current%nval
     return
    end subroutine list_list
  end subroutine SPECGRID_U

  subroutine INVERT_N(wfn,pow2)
   implicit none
   type(WAVEFN),intent(inout) :: wfn
   integer,intent(inout) :: pow2
   integer :: i,j,k,inc,hp2
   real(8) :: xout,yout,zout,intyz,intxyz
   real(8) :: n_t1,n_t2,rp2, minr,maxr,diffr
   write(*,*) "Inverting n-space for wavefunction"
   call cpu_time(n_t1)
!   pow2=16
   pow2=npow2(pow2) ! round up to nearest power of 2
   rp2=pow2*1.;hp2=pow2/2
   allocate(wfn%az(0:pow2))
   allocate(wfn%ayz(0:pow2,0:pow2))
   allocate(wfn%axyz(0:pow2,0:pow2,0:pow2))
   wfn%n_inv=pow2

! Stuff with minr/maxr is yak-shaving.
   wfn%az(0)=-10.;wfn%az(pow2)=+10
   minr=-10.;maxr=10.
   do while (nint_z_FULL(wfn,minr).lt.0.000001)
    minr=minr+0.5 
   enddo;minr=minr-0.5
   do while (nint_z_FULL(wfn,maxr).gt.0.999999)
    maxr=maxr-0.5 
   enddo;maxr=maxr+0.5
   wfn%az(0)=minr;wfn%az(pow2)=maxr
   
   write(*,*) "Z range : ",wfn%az(0),wfn%az(pow2)
   inc=8
! invert nz
   do while (inc.gt.0)
    do k=inc,(pow2-inc),(2*inc)
     call invert_nz( wfn,(k/rp2),wfn%az(k-inc),wfn%az(k+inc),zout)
     wfn%az(k)=zout
    enddo ; inc=inc/2
   enddo
!   write(*,*) "Inverted nz"
! invert nyz
  minr=-10.;maxr=10.
  do while (nint_yz_FULL(wfn,minr,wfn%az(8)).lt.0.000001*nint_yz_FULL(wfn,100._8,wfn%az(8)))
   minr=minr+0.5 
  enddo;minr=minr-0.5
  do while (nint_yz_FULL(wfn,maxr,wfn%az(8)).gt.0.999999*nint_yz_FULL(wfn,100._8,wfn%az(8)))
   maxr=maxr-0.5 
  enddo;maxr=maxr+0.5
  diffr=maxr-minr
  write(*,*) "Y range : ",minr,maxr
  do k=0,pow2
   wfn%ayz(0,k)=minr; wfn%ayz(pow2,k)=maxr; inc=8
   intyz=nint_yz_FULL(wfn,10._8,(wfn%az(k)))
   if (intyz.lt.0.00001) then
    do j=0,pow2;wfn%ayz(j,k)=minr+j*diffr/pow2;enddo
   else
    do while(inc.gt.0)
     do j=inc,(pow2-inc),(2*inc)
!      write(*,*) k,j,"nz = ",k," / 16.  ; target ny : ",intyz*(j/16._8)
      call invert_nyz( wfn,wfn%az(k),intyz*(j/(real(pow2))), wfn%ayz(j-inc,k),wfn%ayz(j+inc,k),yout)
  !subroutine invert_nyz(wfn,z,ny,y1in,y2in,yout)
      wfn%ayz(j,k)=yout
     enddo; inc=inc/2
    enddo
   endif
  enddo

! invert nxyz
  minr=-10.;maxr=10.
  do while (nint_xyz_FULL(wfn,minr,wfn%ayz(8,8),wfn%az(8)).lt.0.000001*nint_xyz_FULL(wfn,100._8,wfn%ayz(8,8),wfn%az(8)))
   minr=minr+0.5 
  enddo;minr=minr-0.5
  do while (nint_xyz_FULL(wfn,maxr,wfn%ayz(8,8),wfn%az(8)).gt.0.999999*nint_xyz_FULL(wfn,100._8,wfn%ayz(8,8),wfn%az(8)))
   maxr=maxr-0.5 
  enddo;maxr=maxr+0.5
  diffr=maxr-minr
  write(*,*) "X range : ",minr,maxr
  do k=0,pow2;do j=0,pow2
   wfn%axyz(0,j,k)=minr; wfn%axyz(pow2,j,k)=maxr; inc=8
   intxyz=nint_xyz_FULL(wfn,10._8,(wfn%ayz(j,k)),(wfn%az(k)))
!   write(*,*) "z : ",i,(i/16.),wfn%az(i),intyz
   if (intxyz.lt.0.00001) then  ! numbers too close to zero to do proper search
    do i=0,pow2;wfn%axyz(i,j,k)=minr+i*diffr/pow2;enddo
   else
    do while(inc.gt.0)
     do i=inc,(pow2-inc),(2*inc)
!      write(*,*) i,j,"nz = ",i," / 16.  ; target ny : ",intyz*(j/16._8)
      call invert_nxyz( wfn,wfn%ayz(j,k),wfn%az(k),intxyz*(i/real(pow2)), wfn%axyz(i-inc,j,k),wfn%axyz(i+inc,j,k),xout)
      wfn%axyz(i,j,k)=xout
     enddo
     inc=inc/2
    enddo
   endif
  enddo;enddo

  call cpu_time(n_t2)
  write(*,*) " Calculated n->r mapping. Time : ",(n_t2-n_t1)

  call fn_map(wfn)

  return
 end subroutine INVERT_N

 subroutine fn_map(wfn)
  implicit none
  type(WAVEFN),intent(inout) :: wfn
  integer :: i,j,k,l,p2,nA
  real(8) :: x,y,z,dens,cbos
  real(8), allocatable :: mos(:),coords(:)
  nA=wfn%nA
  allocate(mos(wfn%nmos))
  allocate(coords(3))
  p2=wfn%n_inv
  allocate(wfn%fn(nA,0:p2,0:p2,0:p2))
  do k=0,p2;z=wfn%az(k)
  do j=0,p2;y=wfn%ayz(j,j)
  do i=0,p2;x=wfn%axyz(i,j,k)
   call calc_MOs(wfn,coords,mos)
   dens=0.
   do l=1,nA;dens=dens+abs(mos(l))**2;enddo
   cbos=sqrt(dens/nA)
   if (cbos.gt.0.000000001) then 
    wfn%fn(:,i,j,k)=mos(1:nA)/cbos
   else
    wfn%fn(:,i,j,k)=1./nA
   endif
  enddo;enddo;enddo
  return
 end subroutine fn_map

  subroutine calc_MOs(wfn,coord,mos)
   implicit none
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: coord(3) 
   real(8),intent(inout) :: mos(wfn%nmos)
   real(8) :: prval
   integer :: i,j,k 
   mos=0.
   do i=1,wfn%nprim
    prval=primval(wfn%p(i),coord)
    do j=1,wfn%nA !wfn%nmos
     mos(j)=mos(j)+prval*wfn%cp(j,i)
    enddo
   enddo
   return
  end subroutine calc_MOs

  real(8) function primval (pr,coord)
   type (PRIM),intent(in) :: pr
   real(8), intent(in) :: coord(3)
   real(8) :: rco(3)
   integer :: i
   rco=coord-pr%xyz; primval=pr%coeff
   do i=1,3
    primval=primval*exp(-1.*pr%expnt*(rco(i)**2))
    if (pr%pow(i).ne.0) primval=primval*(rco(i)**pr%pow(i))
   enddo
   return
  end

  real(8) function primval_1D(pr,r,u)
   implicit none
   real(8),intent(in) :: r
   integer(4),intent(in) :: u ! this is 1, 2 or 3, for x, y or z
   type(PRIM),intent(in) :: pr
   primval_1D=exp(-1.*pr%expnt* (r-pr%xyz(u)) **2) 
   if (pr%pow(u).ne.0) primval_1D=primval_1D*(r-pr%xyz(u))**pr%pow(u)
   return
  end

  subroutine readformchk(cfile,wfn)
   implicit none
   character, intent(in) :: cfile*20
   type (WAVEFN),intent(inout) :: wfn
   character :: csample*20,cend*20,cline*70
   integer(4) :: nsht,nprimtv,nbas,nco,nexp,nmoc,nj
   integer(4),allocatable::sht(:),stbas(:),npl(:),stamap(:)
   real(8),allocatable::rawpr(:),rawpr_p(:),rawco(:),explist(:),moclist(:)
   real(8) :: inttest(6),sm,intval,e1,e2,c1,c2,lintval,z,intg3D
   integer(4) :: i,j,k,l,nlow,isht,ipl,icoef,ico,ibas,iplB,p1,p2
   real(8) :: t1,t2
   call cpu_time(t1)

! stuff for interpreting d orbitals
   expl_6D(1,:)=(/2,0,0/)
   expl_6D(2,:)=(/0,2,0/)
   expl_6D(3,:)=(/0,0,2/)
   expl_6D(4,:)=(/1,1,0/)
   expl_6D(5,:)=(/1,0,1/)
   expl_6D(6,:)=(/0,1,1/)



   open(unit=10,file=cfile,status="old")
   write(*,*) "Reading formatted checkpoint file ",cfile
! get raw numbers from files 
! nfromline goes through until it finds the string, and returns the integer at the end of that line
! (it can rewind). The data headed up by that string is then read in.
   wfn%nat=nfromline("Number of atoms     ",10)
   nj     =nfromline("Atomic numbers      ",10);allocate(wfn%zat(nj));read(10,*) (wfn%zat(k),k=1,nj)
   nj     =nfromline("Current cartesian co",10);allocate(wfn%xyz(nj,3));read(10,*) ( (wfn%xyz(j,k),k=1,3),j=1,wfn%nat)
   wfn%nel=nfromline("Number of electrons ",10)
   wfn%nA=nfromline("Number of alpha elec",10)
   wfn%nB=nfromline("Number of beta elect",10)
   write(*,*) "Number of alpha electrons",wfn%nA
   nsht=nfromline("Shell types         ",10); allocate(sht(nsht));read(10,*) (sht(k),k=1,nsht)
   nsht=nfromline("Number of primitives",10); allocate(npl(nsht));read(10,*) (npl(k),k=1,nsht)
   nsht=nfromline("Shell to atom map   ",10); allocate(stamap(nsht));read(10,*) (stamap(k),k=1,nsht)
   nexp=nfromline   ("Primitive exponents ",10); allocate(explist(nexp));read(10,*) (explist(k),k=1,nexp)
   nbas=0;nlow=10
   do i=1,nsht; nbas=nbas+shtlist(sht(i));nlow=min(nlow,sht(i)); enddo
   nprimtv=nfromline ("Contraction coeffici",10); allocate(rawpr(nprimtv));read(10,*) (rawpr(k),k=1,nprimtv)
   if (nlow.eq.-1) then
    nprimtv=nfromline("P(S=P) Contraction c",10); allocate(rawpr_p(nprimtv));read(10,*) (rawpr_p(k),k=1,nprimtv)
   nco= nfromline("Coordinates of each ",10); allocate(rawco(nco));read(10,*) (rawco(k),k=1,nco)
   nmoc=nfromline("Alpha MO coefficient",10); allocate(moclist(nmoc));read(10,*) (moclist(k),k=1,nmoc)
   endif 

   close(10)
! all the data is now extracted from the file

   wfn%nbas=nbas;wfn%nprim=0

   do i=1,nsht;wfn%nprim=wfn%nprim+shtlist(sht(i))*npl(i);enddo
   write(*,*) "No. of shells: ",nsht,"  Types: ",(sht(k),k=1,nsht)
   write(*,*) "Number of basis functions : ",nbas," Number of primitives : ",wfn%nprim
   allocate(wfn%p(wfn%nprim))
   allocate(wfn%basd(wfn%nbas,wfn%nprim,wfn%nprim))
   allocate(wfn%basprim(wfn%nbas,wfn%nprim))
   wfn%basprim=0.;wfn%basd=0.
! Go through data.
   isht=0;ipl=0;icoef=0;ico=0;ibas=0;iplB=0

! goes through basis function *groups*/*shells*
! e.g. s (1 bf), p (3 bf) sp (4 bf)
   shtloop: do isht=1,nsht;    !write(*,*) "Shell ",isht, " type ",sht(isht)
   ! shtlist(sht(isht)) is the number of basis functions
    do i=1,shtlist(sht(isht))   ! i = basis function in shell.
     ibas=ibas+1
     write(*,*) "Basis function number ",ibas
     do j=(ipl+1),(ipl+npl(isht)) ! j = number of primitive in primitive list
      iplB=iplB+1
!      write(*,*) "Primitive expnt: ",explist(j)
!      write(*,*) "Primitive coeffs: ",rawpr(j),rawpr_p(j)
! now dealing with primitive wfn%prim(iplB)

      wfn%p(iplB)%xyz=rawco( (1+3*(isht-1)):(3*isht) )
      wfn%p(iplB)%expnt=explist(j)
      wfn%p(iplB)%pow=0
      if (sht(isht).eq.1) then
       wfn%p(iplB)%pow(i)=1
      elseif ((i.gt.1).and.(sht(isht).eq.-1)) then
       wfn%p(iplB)%pow(i-1)=1
      elseif (sht(isht).eq.2) then
       wfn%p(iplB)%pow(:)=expl_6D(i,:)
      elseif (sht(isht).eq.-2) then
       wfn%p(iplB)%pow(:)=-i
      endif
      wfn%p(iplB)%coeff=1.
!      write(*,*) "INTCHK: ",int3D(wfn%p(iplB)),intnum(wfn%p(iplB))
      intg3D=1.
      do k=1,3;intg3D=intg3D*Dprimprod (wfn%p(iplB),wfn%p(iplB),k);enddo
      write(*,*) "INTCHK: ",int3D(wfn%p(iplB)),intg3D
      !write(*,*) "INTCHK: ",int3D(wfn%p(iplB)),intnum(wfn%p(iplB)),intg3D
      wfn%p(iplB)%coeff=sqrt(1./intg3D)
!      wfn%p(iplB)%coeff=sqrt(1./int3D(wfn%p(iplB)))
!      write(*,*) "INTCHK: ",int3D(wfn%p(iplB)),"   COEFF: ",wfn%p(iplB)%coeff, "I2: ",&
!         intnum(wfn%p(iplB))
!      write(*,*) "INTCHK: ",int3D(wfn%p(iplB)),"CO:",wfn%p(iplB)%coeff,"TCO: ",&
!          wfn%p(iplB)%coeff*wfn%basprim(ibas,iplB)
      if ((i.eq.1).or.(sht(isht).ge.0)) then
       wfn%basprim(ibas,iplB)=rawpr(j)
      elseif (sht(isht).eq.-1) then
       wfn%basprim(ibas,iplB)=rawpr_p(j)
      endif
!      write(*,*) "INTCHK: ",int3D(wfn%p(iplB)),"CO:",wfn%p(iplB)%coeff,"TCO: ",&
!          wfn%p(iplB)%coeff*wfn%basprim(ibas,iplB)
     enddo
    enddo
    ipl=ipl+npl(isht)
   enddo shtloop
!   write(*,*) nmoc,nmoc/nbas
   wfn%nmos=nmoc/nbas
!   write(*,*) "mo coeffs:"
   allocate(wfn%cb(wfn%nmos,nbas))
   do i=1,wfn%nmos
    do j=1,nbas
     wfn%cb(i,j)=moclist( j + (i-1)*nbas)
    enddo
!    write(*,'(5F8.4)') (wfn%cb(i,:)) 
   enddo
   allocate(wfn%cp(wfn%nmos,wfn%nprim));wfn%cp=0.
   do i=1,wfn%nmos;do j=1,nbas; do k=1,wfn%nprim
    wfn%cp(i,k)=wfn%cp(i,k)+wfn%cb(i,j)*wfn%basprim(j,k)
   enddo;enddo;enddo
!   do i=1,wfn%nmos
!    write(*,*) "MO ",i
!    write(*,'(15F7.3)') wfn%cp(i,:)
!   enddo
   allocate(wfn%dmat(wfn%nprim,wfn%nprim))
   wfn%dmat=0.
! construct density matrix
   do i=1,wfn%nprim; do j=1,wfn%nprim
! for each OCCUPIED mo...
    do k=1,wfn%nA !wfn%nmos
     wfn%dmat(i,j)=wfn%dmat(i,j) + wfn%cp(k,i)*wfn%cp(k,j)
    enddo
!    wfn%dmat(i,j)=wfn%dmat(i,j)*wfn%p(i)%coeff*wfn%p(j)%coeff
   enddo;enddo

! Now density matrix is constructed, create integral of each prim prod over each cartesian component
! This will be done later, repeatedly; doing it now saves repeated calculations 
   allocate(wfn%dpp (wfn%nprim,wfn%nprim,3))
   do i=1,wfn%nprim; do j=1,wfn%nprim; do k=1,3
     wfn%dpp(i,j,k)=Dprimprod(wfn%p(i),wfn%p(j),k)
   enddo;enddo;enddo

! construct each OCC mo's density matrix
   do k=1,wfn%nA;wfn%basd(k,:,:)=0.
    do i=1,wfn%nprim;do j=1,wfn%nprim
     wfn%basd(k,i,j)=wfn%basd(k,i,j)+wfn%cp(k,j)*wfn%cp(k,i)
    enddo;enddo    
   enddo

!   write(*,*) "DMAT, unnorm: "
!   do i=1,wfn%nprim;write(*,*)
!    write(*,'(15F7.3)') wfn%dmat(i,:)
!   enddo

! REMOVED norming dmat to test
!   do i=1,wfn%nprim; do j=1,wfn%nprim
!    wfn%dmat(i,j)=wfn%dmat(i,j)*wfn%p(i)%coeff*wfn%p(j)%coeff
!   enddo;enddo

!   write(*,*) "DMAT, unnorm: "
!   do i=1,wfn%nprim;write(*,*)
!    write(*,'(15F7.3)') wfn%dmat(i,:)
!   enddo
!   sm=0

!   do i=1,wfn%nprim; do j=1,wfn%nprim
!    if (wfn%dmat(i,j).eq.0) cycle
!    intval=1.
!    do k=1,3
!     intval=intval*wfn%dpp(i,j,k)
!!     intval=intval*Dprimprod(wfn%p(i),wfn%p(j),k)
!!     intval=intval*intprod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(k),wfn%p(j)%pow(k))
!!     intval=intval*intprod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(k),wfn%p(j)%pow(k))
!    enddo
!    sm=sm+intval*wfn%dmat(i,j)  * wfn%p(i)%coeff * wfn%p(j)%coeff
!   enddo;enddo

!   write(*,*) "SMCHK ",sm
!   write(*,*) "SMCHK2 ",intdmat(wfn)
!   wfn%nelint=intdmat(wfn)
!   write(*,*) "Alpha electrons (integration check) : ",wfn%nelint,sm
  
!  do l=0,100;z=(l-50)*0.1
!   sm=0.
!   do i=1,wfn%nprim; do j=1,wfn%nprim
!    if (wfn%dmat(i,j).eq.0) cycle
!    intval=nintprod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(k),wfn%p(j)%pow(k),z)
!    do k=1,2
!     intval=intval*intprod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(k),wfn%p(j)%pow(k))
!    enddo
!    sm=sm+intval*wfn%dmat(i,j)
!   enddo;enddo
!!   write(*,'(3F10.5)') z,sm/5.,nint_z(wfn,z)
!  enddo 



!   do i=1,wfn%nbas
!    write(*,*) "bfn ",i
!    write(*,'(6F10.5)') wfn%basprim(i,:)
!   enddo

   do i=1,wfn%nbas
    sm=0.
    do j=1,wfn%nprim;if (wfn%basprim(i,j).eq.0) cycle 
     e1=wfn%p(j)%expnt
!     write(*,*) "Checking prim ",j, "exp ",e1
     intval=1.
     do l=1,3; p1=wfn%p(j)%pow(l)
!      write(*,*) int1D(e1,p1),intprod1D(e1,e1,p1,p1)
      intval=intval*int1D(e1,p1)
     enddo
!     write(*,*) "Total res : ",intval,intval*wfn%p(j)%coeff**2
    enddo
    do j=1,wfn%nprim;if (wfn%basprim(i,j).eq.0) cycle
     e1=wfn%p(j)%expnt
     c1=wfn%p(j)%coeff
     do k=1,wfn%nprim;if (wfn%basprim(i,k).eq.0) cycle
      e2=wfn%p(k)%expnt
      c2=wfn%p(k)%coeff
      intval=1
      do l=1,3;p1=wfn%p(j)%pow(l);p2=wfn%p(k)%pow(l)
!       intval=intval*intprod1D(e1,e2,p1,p2)
       intval=intval*wfn%dpp(j,k,l)
!       intval=intval*Dprimprod(wfn%p(j),wfn%p(l),l)
! this may have been an error - should be p(j) and p(k)
      enddo
!      write(*,*) i,j,k,intval,intval*c1*c2,intval*c1*c2*wfn%basprim(i,j)*wfn%basprim(i,k)
      sm=sm+intval*c1*c2*wfn%basprim(i,j)*wfn%basprim(i,k)
     enddo
    enddo
    if (abs(sm-1.).gt.0.00001) then 
     write(*,*) "ERROR. Orbital ",i," integrates to ",sm 
    else
     write(*,*) "Orbital ",i," integrates to ",sm
    endif
   enddo

   call cpu_time(t2)
   write(*,*) "Readformchk : time : ",t2-t1
!   do i=1,wfn%nprim
!    inttest(4)=int3d(wfn%p(i))
!    inttest(5)=1
!    do j=1,3
!     inttest(j)=int1d(wfn%p(i)%expnt,wfn%p(i)%pow(j))
!     inttest(5)=inttest(5)*inttest(j)
!    enddo
!    inttest(6)=inttest(5)*(wfn%p(i)%coeff)**2
!    write(*,'(6F11.7)') inttest(:)
!   enddo
!   do i=1,wfn%nprim;call printprim(wfn%p(i));enddo
!   do i=1,nprimtv
!    write(*,*) "Prim I: S: ",int1D(explist(i),0),pint1Dn(explist(i),0,100.),pint1D(explist(i),0,100.)
!    write(*,*) "Prim I: P: ",int1D(explist(i),1),pint1Dn(explist(i),1,100.),pint1D(explist(i),1,100.)
!   enddo

   return
  end subroutine readformchk

  real(8) function mo_dens(wfn,mo,x,y,z)
! density of MO at given coordinates
   implicit none
   integer, intent(in) :: mo
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: x,y,z
   integer :: i,j,k,l
   real(8) :: intval
   mo_dens=0.
   do i=1,wfn%nprim;if (wfn%cp(mo,i).eq.0.) cycle   ! double loop over primitives
   do j=1,wfn%nprim;if (wfn%cp(mo,j).eq.0.) cycle   
! for primitive pair i,j: contribution = norm-coeffs of prims * prims in given MO...
    intval=wfn%p(i)%coeff*wfn%p(j)%coeff           
    intval=intval*wfn%cp(mo,i)*wfn%cp(mo,j)
! * product of prims (prim(x) * prim(y) * prim(z) )
    intval=intval*prod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(1),wfn%p(j)%pow(1),x)
    intval=intval*prod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(2),wfn%p(j)%pow(2),y)
    intval=intval*prod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(3),wfn%p(j)%pow(3),z)
    mo_dens=mo_dens+intval
   enddo;enddo
   return
  end function mo_dens

  real(8) function calcdmat(wfn,x,y,z)
   implicit none
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: x,y,z
   integer :: i,j,k,l
   real(8) :: intval,co(3)
   calcdmat=0.;co=(/x,y,z/)
   do i=1,wfn%nprim; do j=1,wfn%nprim
    if (wfn%dmat(i,j).eq.0) cycle
    intval=1.
!    intval=intval*val1D(wfn%p(i)%expnt,wfn%p(i)%pow(1),x,0.0_8)
!    intval=intval*val1D(wfn%p(i)%expnt,wfn%p(i)%pow(2),y,0.0_8)
!    intval=intval*val1D(wfn%p(i)%expnt,wfn%p(i)%pow(3),z,0.0_8)
!    intval=intval*val1D(wfn%p(j)%expnt,wfn%p(j)%pow(1),x,0.0_8)
!    intval=intval*val1D(wfn%p(j)%expnt,wfn%p(j)%pow(2),y,0.0_8)
!    intval=intval*val1D(wfn%p(j)%expnt,wfn%p(j)%pow(3),z,0.0_8)
    intval=intval* primval(wfn%p(i),co) * primval(wfn%p(j),co)
!    intval=intval*prod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(1),wfn%p(j)%pow(1),x)
!    intval=intval*prod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(2),wfn%p(j)%pow(2),y)
!    intval=intval*prod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(3),wfn%p(j)%pow(3),z)
    calcdmat=calcdmat+intval*wfn%dmat(i,j)
   enddo;enddo
   return
  end function calcdmat

  real(8) function intdmat(wfn)
   type(WAVEFN),intent(in) :: wfn
   integer :: i,j,k,l
   real(8) :: sm,intval
   sm=0.
   do i=1,wfn%nprim; do j=1,wfn%nprim
    if (wfn%dmat(i,j).eq.0) cycle
    intval=1.
    do k=1,3
!     intval=intval*intprod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(k),wfn%p(j)%pow(k))
     intval=intval*wfn%dpp(i,j,k)
!     intval=intval*Dprimprod(wfn%p(i),wfn%p(j),k)
    enddo
    sm=sm+intval*wfn%dmat(i,j) * wfn%p(i)%coeff * wfn%p(j)%coeff
   enddo;enddo
   return
  end function intdmat

  real(8) function inv_nz (wfn,nz)
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: nz 
   real(8) :: z1,z2,zt,nz1,nz2,nzt,nzd
   logical :: aCONT
   aCONT=.true.
   z1=-10.;nz1=nint_z(wfn,z1)
   z2=10.;nz2=nint_z(wfn,z2)
   do while (aCONT)
    zt=0.5_8*(z1+z2)
    nzt=nint_z(wfn,zt)
    if (nzt.lt.nz) then
     z1=zt;nz1=nzt
    elseif (nzt.gt.nz) then
     z2=zt;nz2=nzt
    endif
    nzd=abs(nz-nzt)
    aCONT=(0.0001.lt.nzd)
!    write(*,*) zt,nzt
   enddo
   inv_nz=zt
   return
  end function inv_nz

  function get_n (wfn,co) RESULT (nco)
   implicit none
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: co(3)
   real(8),dimension(3) :: nco
   nco(3)=nint_z_FULL(wfn,co(3))
   if (nco(3).eq.0) then
    nco(2)=rangetr((co(2)+10.)/20.)
    nco(1)=rangetr((co(1)+10.)/20.)
    return
   endif
   nco(2)=nint_yz_FULL(wfn,co(2),co(3))/nint_yz(wfn,1000._8,co(3))
   if (nco(2).eq.0) then
    nco(1)=rangetr((co(1)+10.)/20.)
    return
   endif
   nco(1)=nint_xyz_FULL(wfn,co(1),co(2),co(3))/nint_xyz(wfn,1000._8,co(2),co(3))
   return
  end function get_n  
   
  real(8) function rangetr(x)
! truncates to 0-1 range
   implicit none
   real(8),intent(in) :: x
   if (x.lt.0) then; rangetr=0
   elseif (x.gt.1) then; rangetr=1
   else; rangetr=x
   endif;return
  end function rangetr

  subroutine invert_nxyz(wfn,y,z,nx,x1in,x2in,xout)
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: y,z,nx,x1in,x2in
   real(8),intent(out) :: xout
   real(8) :: x1,x2,x3
   real(8) :: n1,n2,n3,n0
   real(8) :: A,B,C,d1,d2,d3,ln1,ln2,lnC,lnx
   integer :: i
   x1=x1in;x2=x2in
   n1=nint_xyz_FULL(wfn,x1,y,z);d1=abs(nx-n1)
   n2=nint_xyz_FULL(wfn,x2,y,z);d2=abs(nx-n2)
   d3=30000.;i=0
!   write(*,*) "Target ny : ",nx
   do while (d3.gt.cutoff )
    i=i+1
!    write(*,*) "It ",i," between ",x1,n1,"   ",x2,n2
!    if (n1.lt.0.0001) then
!     write(*,*) "exp it"
!! n ~ C e^(Bx); ln n = ln C + Bx
!     ln1=log(n1);ln2=log(n2);lnx=log(nx)
!     B=(ln2-ln1)/(x2-x1)
!     lnC=ln1-B*x1    
!! lnx=lnC+B*x3 -> B*x3=lnx-lnC
!     x3=(lnx-lnC)/B 
!    else
    A=(n2-n1)/(x2-x1);n0=n1-A*x1
    x3=((nx-n0)/A)
!    if ((n1.lt.0.15).and.(n2.le.0.51)) then
!!     write(*,*) "adjusting x3 from ",x3
!     x3=x2+(x3-x2)*(1.-sqrt(n2-n1)); !write(*,*) "to : ",x3
!    elseif ((n2.gt.0.85).and.(n1.ge.0.49)) then
!     x3=x1+(x3-x1)*(1.-sqrt(n2-n2))
!    endif
    n3=nint_xyz_FULL(wfn,x3,y,z)
    d3=abs(nx-n3)
!    write(*,*) "Point 1 : ",x1,n1
!    write(*,*) "Point 2 : ",x2,n2
!    write(*,*) "Point 3 : ",x3,n3
    if (d3.lt.cutoff) then
!     write(*,*) i,x3,n3
     exit
    endif
!    write(*,*) "Iteration : ",i
!    write(*,*) "Guesses for x   : ",x1,x2
!    write(*,*) "Corresponding n : ",n1,n2," needs to be : ",nx
!    write(*,*) "New guess :  x :  ",x3,"    n : ",n3
    if (n3.gt.nx) then
!     write(*,*) "replacing guess ",x2," with new guess"
     n2=n3;x2=x3;d2=d3
    else;
!     write(*,*) "replacing guess ",x1," with new guess"
     n1=n3;x1=x3;d1=d3
    endif
   enddo
   xout=x3 
!   write(*,*) xout," after ",i," iterations"
   return
  end subroutine invert_nxyz

  subroutine invert_nyz(wfn,z,ny,y1in,y2in,yout)
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: z,ny,y1in,y2in
   real(8),intent(out) :: yout
   real(8) :: y1,y2,y3
   real(8) :: n1,n2,n3,n0
   real(8) :: A,B,C,d1,d2,d3,ln1,ln2,lnC,lny
   integer :: i
   y1=y1in;y2=y2in
   n1=nint_yz_FULL(wfn,y1,z);d1=abs(ny-n1)
   n2=nint_yz_FULL(wfn,y2,z);d2=abs(ny-n2)
   d3=30000.;i=0
!   write(*,*) "Target ny : ",ny
   do while (d3.gt.cutoff)
    i=i+1
!    write(*,*) "It ",i," between ",y1,n1,"   ",y2,n2
!    if (n1.lt.0.0001) then
!     write(*,*) "exp it"
!! n ~ C e^(By); ln n = ln C + By
!     ln1=log(n1);ln2=log(n2);lny=log(ny)
!     B=(ln2-ln1)/(y2-y1)
!     lnC=ln1-B*y1    
!! lny=lnC+B*y3 -> B*y3=lny-lnC
!     y3=(lny-lnC)/B 
!    else
    A=(n2-n1)/(y2-y1);n0=n1-A*y1
    y3=((ny-n0)/A)
!    if ((n1.lt.0.15).and.(n2.le.0.51)) then
!!     write(*,*) "adjusting y3 from ",y3
!     y3=y2+(y3-y2)*(1.-sqrt(n2-n1)); !write(*,*) "to : ",y3
!    elseif ((n2.gt.0.85).and.(n1.ge.0.49)) then
!     y3=y1+(y3-y1)*(1.-sqrt(n2-n2))
!    endif
    n3=nint_yz_FULL(wfn,y3,z)
    d3=abs(ny-n3)
!    write(*,*) "Point 1 : ",y1,n1
!    write(*,*) "Point 2 : ",y2,n2
!    write(*,*) "Point 3 : ",y3,n3
    if (d3.lt.cutoff) then
!     write(*,*) i,y3,n3
     exit
    endif
!    write(*,*) "Iteration : ",i
!    write(*,*) "Guesses for y   : ",y1,y2
!    write(*,*) "Corresponding n : ",n1,n2," needs to be : ",ny
!    write(*,*) "New guess :  y :  ",y3,"    n : ",n3
    if (n3.gt.ny) then
!     write(*,*) "replacing guess ",y2," with new guess"
     n2=n3;y2=y3;d2=d3
    else;
!     write(*,*) "replacing guess ",y1," with new guess"
     n1=n3;y1=y3;d1=d3
    endif
   enddo
   yout=y3 
!   write(*,*) yout," after ",i," iterations"
   return
  end subroutine invert_nyz

  subroutine invert_nz(wfn,nz,z1in,z2in,zout)
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: nz,z1in,z2in
   real(8),intent(out) :: zout
   real(8) :: z1,z2,z3
   real(8) :: n1,n2,n3,n0
   real(8) :: A,B,C,d1,d2,d3,ln1,ln2,lnC,lnz
   integer :: i
   z1=z1in;z2=z2in
   n1=nint_z_FULL(wfn,z1);d1=abs(nz-n1)
   n2=nint_z_FULL(wfn,z2);d2=abs(nz-n2)
   d3=30000.;i=0
   do while (d3.gt.cutoff)
    i=i+1
!    write(*,*) "It ",i," between ",z1,n1,"   ",z2,n2
!    if (n1.lt.0.0001) then
!     write(*,*) "exp it"
!! n ~ C e^(Bz); ln n = ln C + Bz
!     ln1=log(n1);ln2=log(n2);lnz=log(nz)
!     B=(ln2-ln1)/(z2-z1)
!     lnC=ln1-B*z1    
!! lnz=lnC+B*z3 -> B*z3=lnz-lnC
!     z3=(lnz-lnC)/B 
!    else
    A=(n2-n1)/(z2-z1);n0=n1-A*z1
    z3=((nz-n0)/A)
!    if ((n1.lt.0.15).and.(n2.le.0.51)) then
!!     write(*,*) "adjusting z3 from ",z3
!     z3=z2+(z3-z2)*(1.-sqrt(n2-n1)); !write(*,*) "to : ",z3
!    elseif ((n2.gt.0.85).and.(n1.ge.0.49)) then
!     z3=z1+(z3-z1)*(1.-sqrt(n2-n2))
!    endif
    n3=nint_z_FULL(wfn,z3)
    d3=abs(nz-n3)
!    write(*,*) "Point 1 : ",z1,n1
!    write(*,*) "Point 2 : ",z2,n2
!    write(*,*) "Point 3 : ",z3,n3
    if (d3.lt.cutoff) then
!     write(*,*) i,z3,n3
     exit
    endif
!    write(*,*) "Iteration : ",i
!    write(*,*) "Guesses for z   : ",z1,z2
!    write(*,*) "Corresponding n : ",n1,n2," needs to be : ",nz
!    write(*,*) "New guess :  z :  ",z3,"    n : ",n3
    if (n3.gt.nz) then
!     write(*,*) "replacing guess ",z2," with new guess"
     n2=n3;z2=z3;d2=d3
    else;
!     write(*,*) "replacing guess ",z1," with new guess"
     n1=n3;z1=z3;d1=d3
    endif
   enddo
   zout=z3 
!   write(*,*) zout," after ",i," iterations"
   return
  end subroutine invert_nz

  real(8) function nint_z_FULL (wfn,z)
! This function needs to be divided by wfn%nel (done internally)
! ...changed to wfn%nA
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: z 
   integer :: i,j,k,l
   real(8) :: intval
   nint_z_FULL=0.
! cycle over primitives
   do i=1,wfn%nprim; do j=1,wfn%nprim
    if (wfn%dmat(i,j).eq.0) cycle
    intval=fprimprod(wfn%p(i),wfn%p(j),z,3)
    do k=1,2
     intval=intval*wfn%dpp(i,j,k)
!     intval=intval*Dprimprod(wfn%p(i),wfn%p(j),k)
    enddo
    intval=intval*wfn%p(i)%coeff*wfn%p(j)%coeff
    nint_z_FULL=nint_z_FULL+intval*wfn%dmat(i,j)
   enddo;enddo
   nint_z_FULL=nint_z_FULL/wfn%nA
   return
  end function nint_z_FULL

  real(8) function dnint_z_FULL (wfn,z)
! This function needs to be divided by wfn%nel (done internally)
! ...changed to wfn%nA
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: z 
   integer :: i,j,k,l
   real(8) :: intval
   dnint_z_FULL=0.
! cycle over primitives
   do i=1,wfn%nprim; do j=1,wfn%nprim
    if (wfn%dmat(i,j).eq.0) cycle
    intval=primval_1D(wfn%p(i),z,3)*primval_1D(wfn%p(j),z,3)
    do k=1,2
     intval=intval*wfn%dpp(i,j,k)
!     intval=intval*Dprimprod(wfn%p(i),wfn%p(j),k)
    enddo
    intval=intval*wfn%p(i)%coeff*wfn%p(j)%coeff
    dnint_z_FULL=dnint_z_FULL+intval*wfn%dmat(i,j)
   enddo;enddo
   dnint_z_FULL=dnint_z_FULL/wfn%nA
   return
  end function dnint_z_FULL

  real(8) function nint_u_FULL (wfn,z,u)
! This function needs to be divided by wfn%nel (done internally)
! ...changed to wfn%nA
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: z 
   integer,intent(in) :: u
   integer :: i,j,k,l,v,w
   real(8) :: intval
   nint_u_FULL=0.
   v=u-1;w=u-2;if (v.lt.1) v=v+3; if (w.lt.1) w=w+3
! cycle over primitives
   do i=1,wfn%nprim; do j=1,wfn%nprim
    if (wfn%dmat(i,j).eq.0) cycle
    intval=fprimprod(wfn%p(i),wfn%p(j),z,u)
    intval=intval*wfn%dpp(i,j,v)*wfn%dpp(i,j,w)
!     intval=intval*Dprimprod(wfn%p(i),wfn%p(j),k)
    intval=intval*wfn%p(i)%coeff*wfn%p(j)%coeff
    nint_u_FULL=nint_u_FULL+intval*wfn%dmat(i,j)
   enddo;enddo
   nint_u_FULL=nint_u_FULL/wfn%nA
   return
  end function nint_u_FULL

  real(8) function dnint_yz_FULL (wfn,y,z)
! Derivative!
! This function needs to be divided by nint_z(wfn,z)
! NOT done internally!
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: y,z 
   integer :: i,j
   real(8) :: intval
   dnint_yz_FULL=0.
   do i=1,wfn%nprim; do j=1,wfn%nprim
    if (wfn%dmat(i,j).eq.0) cycle
    intval=primval_1D(wfn%p(i),z,3)*primval_1D(wfn%p(j),z,3)
    intval=intval*primval_1D(wfn%p(i),y,2)*primval_1D(wfn%p(j),y,2)
!    intval=intval*fprimprod(wfn%p(i),wfn%p(j),y,2)
    intval=intval*wfn%dpp(i,j,1)
!    intval=intval*Dprimprod(wfn%p(i),wfn%p(j),1)
    intval=intval*wfn%p(i)%coeff*wfn%p(j)%coeff
    dnint_yz_FULL=dnint_yz_FULL+intval*wfn%dmat(i,j)
   enddo;enddo
   return
  end function dnint_yz_FULL 


  real(8) function nint_yz_OPT (wfn,y,kz)
! This function needs to be divided by nint_z(wfn,z)
! NOT done internally!
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: y
   integer,intent(in) :: kz
   integer :: i,j
   real(8) :: intval
   nint_yz_OPT=0.
   do i=1,wfn%nprim; do j=1,wfn%nprim
    if (wfn%dmat(i,j).eq.0) cycle
    intval=wfn%pp_z(i,j,kz)
!    intval=primval_1D(wfn%p(i),z,3)*primval_1D(wfn%p(j),z,3)
    intval=intval*fprimprod(wfn%p(i),wfn%p(j),y,2)
    intval=intval*wfn%dpp(i,j,1)
!    intval=intval*Dprimprod(wfn%p(i),wfn%p(j),1)
    intval=intval*wfn%p(i)%coeff*wfn%p(j)%coeff
    nint_yz_OPT=nint_yz_OPT+intval*wfn%dmat(i,j)
   enddo;enddo
   return
  end function nint_yz_OPT

  real(8) function nint_yz_FULL (wfn,y,z)
! This function needs to be divided by nint_z(wfn,z)
! NOT done internally!
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: y,z 
   integer :: i,j
   real(8) :: intval
   nint_yz_FULL=0.
   do i=1,wfn%nprim; do j=1,wfn%nprim
    if (wfn%dmat(i,j).eq.0) cycle
    intval=primval_1D(wfn%p(i),z,3)*primval_1D(wfn%p(j),z,3)
    intval=intval*fprimprod(wfn%p(i),wfn%p(j),y,2)
    intval=intval*wfn%dpp(i,j,1)
!    intval=intval*Dprimprod(wfn%p(i),wfn%p(j),1)
    intval=intval*wfn%p(i)%coeff*wfn%p(j)%coeff
    nint_yz_FULL=nint_yz_FULL+intval*wfn%dmat(i,j)
   enddo;enddo
   return
  end function nint_yz_FULL

  real(8) function dnint_xyz_FULL (wfn,x,y,z)
! This function needs to be divided by nint_yz(wfn,y,z)
! NOT done internally!
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: x,y,z 
   integer :: i,j
   real(8) :: intval
   dnint_xyz_FULL=0.
   do i=1,wfn%nprim; do j=1,wfn%nprim
    if (wfn%dmat(i,j).eq.0) cycle
    intval=       primval_1D(wfn%p(i),z,3)*primval_1D(wfn%p(j),z,3)
    intval=intval*primval_1D(wfn%p(i),y,2)*primval_1D(wfn%p(j),y,2)
    intval=intval*primval_1D(wfn%p(i),x,1)*primval_1D(wfn%p(j),x,1)
!    intval=intval*fprimprod(wfn%p(i),wfn%p(j),x,1)
    intval=intval*wfn%p(i)%coeff*wfn%p(j)%coeff
    dnint_xyz_FULL=dnint_xyz_FULL+intval*wfn%dmat(i,j)
   enddo;enddo
   return
  end function dnint_xyz_FULL

  real(8) function dnint_xyz_OPT (wfn,x,jy,kz)
! This function needs to be divided by nint_yz(wfn,y,z)
! NOT done internally!
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: x 
   integer,intent(in) :: jy,kz
   integer :: i,j
   real(8) :: intval
   dnint_xyz_OPT=0.
   do i=1,wfn%nprim; do j=1,wfn%nprim
    if (wfn%dmat(i,j).eq.0) cycle
    intval=wfn%pp_yz(i,j,jy,kz)
    intval=intval*primval_1D(wfn%p(i),x,1)*primval_1D(wfn%p(j),x,1)
!    intval=intval*fprimprod(wfn%p(i),wfn%p(j),x,1)
    intval=intval*wfn%p(i)%coeff*wfn%p(j)%coeff
    dnint_xyz_OPT=dnint_xyz_OPT+intval*wfn%dmat(i,j)
   enddo;enddo
   return
  end function dnint_xyz_OPT

  subroutine nint_xyz_DEBUG (wfn,x,y,z,j,k)
! at the time of writing there are problems with nint_xyz_OPT
! this subroutine is for debugging (needs to be a subroutine
! 'cos fortran doesn't allow output from a function, highly annoying)
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: x,y,z
   integer,intent(in) :: j,k
   real(8) :: ivO,ivF ! working values of integral: intval for OPT and FULL
   real(8) :: fvO,fvF ! final values, summed over integrals  
   integer :: ii,jj
   fvO=0.;fvF=0.
   open(unit=56,file="intXYZ.dump",status="replace")
   write(56,*) "Dumping for j,k=",j,k,y,z
   do ii=1,wfn%nprim;do jj=1,wfn%nprim
    if (wfn%dmat(ii,jj).eq.0) cycle
    ivO=wfn%pp_yz(ii,jj,j,k)
    ivF=    primval_1D(wfn%p(ii),z,3)*primval_1D(wfn%p(jj),z,3)
    ivF=ivF*primval_1D(wfn%p(ii),y,2)*primval_1D(wfn%p(jj),y,2)
    write(56,'(2I4,2F8.3,4X,F9.4)') ii,jj,ivO,ivF,abs(ivO-ivF)
    ivO=ivO*fprimprod(wfn%p(ii),wfn%p(jj),x,1)
    ivF=ivF*fprimprod(wfn%p(ii),wfn%p(jj),x,1)
    fvO=fvO+ivO*wfn%dmat(ii,jj)
    fvF=fvF+ivF*wfn%dmat(ii,jj)
   enddo;enddo

   close(56)
   stop
   return
  end subroutine nint_xyz_DEBUG

  real(8) function nint_xyz_OPT (wfn,x,jy,kz)
! This function needs to be divided by nint_yz(wfn,y,z)
! NOT done internally!
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: x 
   integer,intent(in) :: jy,kz
   integer :: i,j
   real(8) :: intval
   nint_xyz_OPT=0.
   do i=1,wfn%nprim; do j=1,wfn%nprim
    if (wfn%dmat(i,j).eq.0) cycle
    intval=wfn%pp_yz(i,j,jy,kz)
!    intval=wfn%pp_z(i,j,kz) * wfn%pp_yz(i,j,jy,kz)
    intval=intval*fprimprod(wfn%p(i),wfn%p(j),x,1)
    intval=intval*wfn%p(i)%coeff*wfn%p(j)%coeff
    nint_xyz_OPT=nint_xyz_OPT+intval*wfn%dmat(i,j)
   enddo;enddo
   return
  end function nint_xyz_OPT

  real(8) function nint_xyz_FULL (wfn,x,y,z)
! This function needs to be divided by nint_yz(wfn,y,z)
! NOT done internally!
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: x,y,z 
   integer :: i,j
   real(8) :: intval
   nint_xyz_FULL=0.
   do i=1,wfn%nprim; do j=1,wfn%nprim
    if (wfn%dmat(i,j).eq.0) cycle
    intval=       primval_1D(wfn%p(i),z,3)*primval_1D(wfn%p(j),z,3)
    intval=intval*primval_1D(wfn%p(i),y,2)*primval_1D(wfn%p(j),y,2)
    intval=intval*fprimprod(wfn%p(i),wfn%p(j),x,1)
    intval=intval*wfn%p(i)%coeff*wfn%p(j)%coeff
    nint_xyz_FULL=nint_xyz_FULL+intval*wfn%dmat(i,j)
   enddo;enddo
   return
  end function nint_xyz_FULL

  real(8) function nint_z (wfn,z)
! This function needs to be divided by wfn%nA (done internally)
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: z 
   integer :: i,j,k,l
   real(8) :: intval
   nint_z=0.
   do i=1,wfn%nprim; do j=1,wfn%nprim
    if (wfn%dmat(i,j).eq.0) cycle
    intval=nintprod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(3),wfn%p(j)%pow(3),z)
    do k=1,2
     intval=intval*intprod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(k),wfn%p(j)%pow(k))
    enddo
    nint_z=nint_z+intval*wfn%dmat(i,j)
   enddo;enddo
   nint_z=nint_z/wfn%nA
   return
  end function nint_z

  real(8) function nint_yz (wfn,y,z)
! This function needs to be divided by nint_z(wfn,z)
! NOT done internally!
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: y,z 
   integer :: i,j
   real(8) :: intval
   nint_yz=0.
   do i=1,wfn%nprim; do j=1,wfn%nprim
    if (wfn%dmat(i,j).eq.0) cycle
    intval=prod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(3),wfn%p(j)%pow(3),z)
    intval=intval*nintprod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(2),wfn%p(j)%pow(2),y)
    intval=intval* intprod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(1),wfn%p(j)%pow(1))
    nint_yz=nint_yz+intval*wfn%dmat(i,j)
   enddo;enddo
   return
  end function nint_yz

  real(8) function dnint_xyz (wfn,x,y,z)
! This is unintegrated function, i.e. (d/dx)R_xyz
! This function needs to be divided by nint_yz(wfn,y,z)
! NOT done internally!
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: x,y,z 
   integer :: i,j
   real(8) :: intval
   dnint_xyz=0.
   do i=1,wfn%nprim; do j=1,wfn%nprim
    if (wfn%dmat(i,j).eq.0) cycle
!    intval=nintprod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(1),wfn%p(j)%pow(1),x)
    intval=prod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(1),wfn%p(j)%pow(1),x)
    intval=intval*prod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(2),wfn%p(j)%pow(2),y)
    intval=intval*prod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(3),wfn%p(j)%pow(3),z)
    dnint_xyz=dnint_xyz+intval*wfn%dmat(i,j)
   enddo;enddo
   return
  end function dnint_xyz

  real(8) function nint_xyz (wfn,x,y,z)
! This function needs to be divided by nint_yz(wfn,y,z)
! NOT done internally!
   type(WAVEFN),intent(in) :: wfn
   real(8),intent(in) :: x,y,z 
   integer :: i,j
   real(8) :: intval
   nint_xyz=0.
   do i=1,wfn%nprim; do j=1,wfn%nprim
    if (wfn%dmat(i,j).eq.0) cycle
    intval=nintprod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(1),wfn%p(j)%pow(1),x)
    intval=intval*prod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(2),wfn%p(j)%pow(2),y)
    intval=intval*prod1D(wfn%p(i)%expnt,wfn%p(j)%expnt,wfn%p(i)%pow(3),wfn%p(j)%pow(3),z)
    nint_xyz=nint_xyz+intval*wfn%dmat(i,j)
   enddo;enddo
   return
  end function nint_xyz


  real(8) function prod1D(exp1,exp2,pow1,pow2,x)
   real(8),intent(in) :: exp1,exp2,x
   integer(4),intent(in) :: pow1,pow2
   integer :: p12
   p12=pow1+pow2;prod1D=exp(-1.*(exp1+exp2)*x*x)
   if (p12.gt.0) prod1D=prod1D*(x**p12)
   return
  end function prod1D

  real(8) function full_prod1D(prim1,prim2,r,u,aINT)
   implicit none
   real(8),intent(in) :: r
   integer(4),intent(in) :: u ! this is 1, 2 or 3, for x, y or z
   type(PRIM),intent(in) :: prim1,prim2
   logical,intent(in) :: aINT
   real(8) :: r1,r2,r3,rd,scr_exp,A,B,rC
   integer :: i,j,k
   r1=r-prim1%xyz(u);r2=r-prim2%xyz(u)
   A=prim1%expnt;B=prim2%expnt
   full_prod1D=r1**prim1%pow(u) * r2**prim2%pow(u)
!   rd=0.;do i=1,3;rd=rd+(prim1%xyz(u)-prim2%xyz(u))**2;enddo;rd=sqrt(rd)
   rd=abs(prim1%xyz(u)-prim2%xyz(u))
   if (rd.lt.0.00001) then ! if prims have same centre, then...
    scr_exp=exp(-1.*r1*r1*(A+B)) 
   else                    ! otherwise, calc. product of gaussians
    scr_exp=exp( - (A*B/(A+B)) * (prim1%xyz(u)-prim2%xyz(u))**2 )
    rC=(prim1%xyz(u)*A+prim2%xyz(u)*B)/(A+B)
    scr_exp=scr_exp*exp(-1.*(A+B)*(r-rC)**2)
   endif
   full_prod1D=full_prod1D*scr_exp
   return
  end function full_prod1D

! this is a function version of PRIMPROD, taking 2 primitives, a distance, a cartesian component
! and doing indefinite integration of the product of the primitives up to the specified distance
! along the cartesian component 
  real(8) function fprimprod(prim1,prim2,r,u) result(intp)
   implicit none
   real(8),intent(in) :: r
   integer(4),intent(in) :: u ! this is 1, 2 or 3, for x, y or z
   type(PRIM),intent(in) :: prim1,prim2
   real(8) :: r1,r2,r3,rd,scr_exp,A,B,rA,rB,rC,prefac,c_i(0:10),c_exp(0:10),sm,rAC,rBC,coeffp
   real(8) :: gv,gm,rcalc,radd
   integer :: i,j,k,p1,p2
! calculate distances // extract variables from PRIM types:
  !   coeffp=prim1%coeff * prim2%coeff
   coeffp=1. !! I *think* the coefficients of the prims get into the dmatrix somehow...
             !! They should be in the prim coefficients in the basis function. But not completely certain.
             !! Coefficients taken out of dmat, now thrown in in nint_u functions
   rA=prim1%xyz(u); rB=prim2%xyz(u)
   r1=r-prim1%xyz(u);r2=r-prim2%xyz(u)
   A=prim1%expnt;B=prim2%expnt
   p1=prim1%pow(u); p2=prim2%pow(u)
   rd=abs(prim1%xyz(u)-prim2%xyz(u))
! if primitives are at the same location, this is easy....
   if (abs(rd).lt.0.0000000001) then  ! primitives at same location
    intp=pint1D_rec( (A+B),(p1+p2),r1) * coeffp
    return
   endif
! not, calculate prefactor... including coefficient product
   prefac=exp( - (A*B/(A+B)) * (prim1%xyz(u)-prim2%xyz(u))**2 )
   prefac=prefac*coeffp 
! and new centre of gaussian:
   rC=(prim1%xyz(u)*A+prim2%xyz(u)*B)/(A+B); r3=r-rC
! (r-rA)**n -> ( (r-rC) - (rA-rC) ) **n 
   rAC=rA-rC
   rBC=rB-rC
! if no cartesian components, this is a simple case...
   if ((p1+p2).eq.0) then
     intp=prefac*pint1D_rec( (A+B),0,r3) ; return
   endif
! need expansion coefficients to expand ( (r-rC) - (rA-rC)) **p1 * ( (r-rC)-(rB-rC) **p2
   call EXPANDX( -rAC,-rBC ,p1,p2,c_exp)
! need integrals for (r-rC)**p1 * gaussian (around rC)
   c_i=pint1D_rArr( (A+B),(p1+p2),r3)
! put the two together ....
   intp=0.
   do i=0,(p1+p2);  intp=intp+c_i(i)*c_exp(i);  enddo
   intp=intp*prefac
   return
  end function fprimprod

! this is the definite integral version of FPRIMPROD
  real(8) function Dprimprod(prim1,prim2,u) result(intp)
   implicit none
!   real(8),intent(in) :: r
   integer(4),intent(in) :: u ! this is 1, 2 or 3, for x, y or z
   type(PRIM),intent(in) :: prim1,prim2
   real(8) :: r1,r2,r3,rd,scr_exp,A,B,rA,rB,rC,prefac,c_i(0:10),c_exp(0:10),sm,rAC,rBC,coeffp
   real(8) :: gv,gm,rcalc,radd
   integer :: i,j,k,p1,p2
! calculate distances // extract variables from PRIM types:
   rA=prim1%xyz(u); rB=prim2%xyz(u)
   A=prim1%expnt;B=prim2%expnt
   p1=prim1%pow(u); p2=prim2%pow(u)
   rd=abs(prim1%xyz(u)-prim2%xyz(u))
! need integrals for (r-rC)**p1 * gaussian (around rC)
   c_i=dint1D_rArr ( (A+B),(p1+p2)) 
! if primitives are at the same location, this is easy....
   if (abs(rd).lt.0.0000000001) then  ! primitives at same location
    intp=c_i(p1+p2)
    return
   endif
! not, calculate prefactor... including coefficient product
   prefac=exp( - (A*B/(A+B)) * (prim1%xyz(u)-prim2%xyz(u))**2 )
! and new centre of gaussian:
   rC=(prim1%xyz(u)*A+prim2%xyz(u)*B)/(A+B)
! (r-rA)**n -> ( (r-rC) - (rA-rC) ) **n 
   rAC=rA-rC
   rBC=rB-rC
! if no cartesian components, this is a simple case...
   if ((p1+p2).eq.0) then
     intp=prefac*c_i(0); return
   endif
! need expansion coefficients to expand ( (r-rC) - (rA-rC)) **p1 * ( (r-rC)-(rB-rC) **p2
   call EXPANDX( -rAC,-rBC ,p1,p2,c_exp)
   intp=0.
   do i=0,(p1+p2);  intp=intp+c_i(i)*c_exp(i);  enddo
   intp=intp*prefac
   return
  end function Dprimprod

! this subroutine takes two primitives; a single distance; the cartesian component; and returns two variables:
! valp is the product of the two primitives, and intp is the indefinite integral of the two primitives up to r 
  subroutine primprod(prim1,prim2,r,u,valp,intp) 
   implicit none
   real(8),intent(in) :: r
   real(8),intent(out) :: valp,intp
   integer(4),intent(in) :: u ! this is 1, 2 or 3, for x, y or z
   type(PRIM),intent(in) :: prim1,prim2
   real(8) :: r1,r2,r3,rd,scr_exp,A,B,rA,rB,rC,prefac,c_i(0:10),c_exp(0:10),sm,rAC,rBC
   real(8) :: gv,gm,rcalc,radd
   integer :: i,j,k,p1,p2
! calculate distances // extract variables from PRIM types:
   rA=prim1%xyz(u); rB=prim2%xyz(u)
   r1=r-prim1%xyz(u);r2=r-prim2%xyz(u)
   A=prim1%expnt;B=prim2%expnt
   p1=prim1%pow(u); p2=prim2%pow(u)
   rd=abs(prim1%xyz(u)-prim2%xyz(u))
! if primitives are at the same location, this is easy....
   if (abs(rd).lt.0.0000000001) then  ! primitives at same location
    valp=exp(-1.*(A+B)*r1*r1);  if ( (p1+p2).gt.0) valp=valp*(r1**(p1+p2)) 
    intp=pint1D_rec( (A+B),(p1+p2),r1)
    return
   endif
! not, calculate prefactor...
   prefac=exp( - (A*B/(A+B)) * (prim1%xyz(u)-prim2%xyz(u))**2 )
! and new centre of gaussian:
   rC=(prim1%xyz(u)*A+prim2%xyz(u)*B)/(A+B); r3=r-rC
! (r-rA)**n -> ( (r-rC) - (rA-rC) ) **n 
   rAC=rA-rC
   rBC=rB-rC
   gv=exp(-1.*A*r1*r1)*exp(-1.*B*r2*r2)
!   gm=prefac*exp(-1.*(A+B)*r3*r3)
! if no cartesian components, this is a simple case...
   if ((p1+p2).eq.0) then
     valp=gv ; intp=prefac*pint1D_rec( (A+B),0,r3) ; return
   endif
! if cartesian components, calculate... 
   rcalc=(r1**p1)*(r2**p2);   valp=rcalc*gv
! need expansion coefficients to expand ( (r-rC) - (rA-rC)) **p1 * ( (r-rC)-(rB-rC) **p2
   call EXPANDX( -rAC,-rBC ,p1,p2,c_exp)
! need integrals for (r-rC)**p1 * gaussian (around rC)
   c_i=pint1D_rArr( (A+B),(p1+p2),r3)
! put the two together ....
   intp=0.
   do i=0,(p1+p2);  intp=intp+c_i(i)*c_exp(i);  enddo
   intp=intp*prefac
   return
  end subroutine primprod


  real(8) function full_nprod1D(prim1,prim2,r,u) 
   implicit none
   real(8),intent(in) :: r
   integer(4),intent(in) :: u ! this is 1, 2 or 3, for x, y or z
   type(PRIM),intent(in) :: prim1,prim2
   real(8) :: r1,r2,r3,rd,scr_exp,A,B,rC,prefac,c_i(0:10),c_exp(0:10),sm
   integer :: i,j,k,p1,p2
   r1=r-prim1%xyz(u);r2=r-prim2%xyz(u)
   A=prim1%expnt;B=prim2%expnt
   p1=prim1%pow(u)
   p2=prim2%pow(u)
 !  full_prod1D=r1**prim1%pow(u) * r2**prim2%pow(u)
   rd=(prim1%xyz(u)-prim2%xyz(u))
   if (abs(rd).lt.0.0000000001) then
    full_nprod1D=pint1D_rec( (A+B),(p1+p2),r1)
   else
    rC=(prim1%xyz(u)*A+prim2%xyz(u)*B)/(A+B)
    r3=r-rC
    prefac=exp( - (A*B/(A+B)) * (prim1%xyz(u)-prim2%xyz(u))**2 )
    c_i=pint1D_rArr( (A+B),(p1+p2),r3)
    call EXPANDX( (prim1%xyz(u)-rC),(prim2%xyz(u)-rC),p1,p2,c_exp)
    sm=0.
    do i=1,(p1+p2)
     sm=sm+c_i(i)*c_exp(i)
    enddo
    full_nprod1D=sm
   endif
   return
  end function full_nprod1D

  real(8) function intprod1D(exp1,exp2,pow1,pow2)
   real(8),intent(in) :: exp1,exp2
   integer(4),intent(in) :: pow1,pow2
   real(8) :: e12
   integer :: p12
   p12=pow1+pow2;e12=exp1+exp2
   if (mod(p12,2).eq.1) then;intprod1D=0.
   elseif (p12.eq.0) then; intprod1D=sqrt(pi/e12)
   elseif (p12.eq.2) then; intprod1D=sqrt(pi/(4.*e12*e12*e12))
   endif
   return
  end function intprod1D

  real(8) function nintprod1D(exp1,exp2,pow1,pow2,x)
   real(8),intent(in) :: exp1,exp2,x
   integer(4),intent(in) :: pow1,pow2
   real(8) :: e12
   integer :: p12
   p12=pow1+pow2;e12=exp1+exp2
   if (p12.eq.0) then;nintprod1D=pint1D( 0.5*e12 ,0,x)
   elseif (p12.eq.2) then;nintprod1D=pint1D( 0.5*e12,1 ,x)
   elseif (p12.eq.1) then;nintprod1D=pint1D( e12,-1,x)
   endif
   return
  end function nintprod1D

  subroutine primprod_ND(exp1,exp2,pow1,pow2)
   real(8),intent(in) :: exp1,exp2
   integer(4),intent(in) :: pow1,pow2
   integer :: i,j,k,p12
   real(8) :: x,v,sm,ant
   p12=pow1+pow2;sm=0.
   do i=-350000,350000;x=0.00001*i
    v=exp(-1.*(exp1+exp2)*x*x)
    if (p12.gt.0) v=v*(x**p12)
    sm=sm+0.00001*v
    ant=nintprod1D(exp1,exp2,pow1,pow2,x)
!    if (p12.eq.0) then;ant=pint1D( (0.5*(exp1+exp2)),0,x)
!    elseif (p12.eq.2) then;ant=pint1D( (0.5*(exp1+exp2)),1,x)
!    elseif (p12.eq.1) then;ant=pint1D( (exp1+exp2),-1,x)
!    endif
    if ((mod(i,10000)).eq.0)write(*,'(4F10.5)') x,sm,ant,abs(sm-ant)
!    write(*,'(3F10.5)') x,(val1D(exp1,pow1,x,0.)*val1D(exp2,pow2,x,0.)),v
   enddo 
   write(*,*) "At end : "
   write(*,'(3F11.7)') sm,ant,intprod1D(exp1,exp2,pow1,pow2)
   return
  end subroutine primprod_ND

  real(8) function int3D(pr)
   type(PRIM),intent(in) :: pr
   real(8)::res(3)
   integer :: i,j,k
   do i=1,3
    if (pr%pow(i).eq.0) then
     res(i)=sqrt(PI/(2.*pr%expnt))
    elseif (pr%pow(i).eq.1) then
     res(i)=sqrt(PI/(32.*(pr%expnt**3)))
    endif
   enddo
   int3D=res(1)*res(2)*res(3)*pr%coeff*pr%coeff
!   write(*,*) int3D,"    ",sqrt( (PI/(2.*pr%expnt))**3)
   return
  end function int3D

  real(8) function pint1D(expnt,pow,x)
   real(8),intent(in) :: expnt,x
   integer,intent(in) :: pow
   if (pow.eq.0) then
    pint1D=(sqrt(pi/(2*expnt)))*0.5*(1.+erf(x*sqrt(2*expnt)))
   elseif (pow.eq.1) then
    pint1D=(sqrt(pi/(2*expnt)))*0.5*(1.+erf(x*sqrt(2*expnt)))
    pint1D=pint1D/(4*expnt)
    pint1D=pint1D-(x/(4*expnt))*exp(-2.*x*x*expnt)
   elseif (pow.eq.-1) then
    pint1D=(-0.5/expnt)*exp(-1*expnt*x*x)
   endif
   return
  end

  real(8) function pint1Dn(expnt,pow,x)
   real(8),intent(in) :: expnt,x
   integer,intent(in) :: pow
   integer :: i
   real(8) :: xs,dx,val
   pint1Dn=0.
   if (x.lt.-6.) return
   dx=(x+6.)/5000.
   do i=1,5000;xs=i*dx-6.
    val=(val1D(expnt,pow,xs,0._8))**2
!    val=exp(-2.*expnt*xs*xs)
!    if (pow.gt.0) val=val*(xs*xs)
    pint1Dn=pint1Dn+val*dx
   enddo
   return
  end function pint1Dn

  real(8) function val1D(expnt, pow, x,x0)
   real(8),intent(in) :: expnt,x,x0
   integer,intent(in) :: pow
   integer :: i
   real(8) :: dx,val
   dx=x-x0
   val1D=exp(-1.*expnt*dx*dx)
   if (pow.eq.0) then
    return
   else
    val1D=val1D*(dx**pow)
    return
   endif
   return
  end function val1D


  real(8) function int1D(expnt,pow)
   real(8),intent(in) :: expnt
   integer,intent(in) :: pow
   if (pow.eq.0) then
    int1D=sqrt(PI/(2.*expnt))
   elseif (pow.eq.1) then
    int1D=sqrt(PI/(32.*expnt*expnt*expnt))
   endif 
   return
  end function int1D

  real(8) function int1Dn(expnt,pow)
   real(8),intent(in) :: expnt
   integer,intent(in) :: pow
   integer :: i
   real(8) :: x,val
   int1Dn=0.
   do i=1,1000;x=(i-500)*0.04
    val=exp(-2.*expnt*x*x)
    if (pow.gt.0) val=val*(x*x)
    int1Dn=int1Dn+val*0.04
   enddo
   return
  end function int1Dn



  subroutine intcheck(expnt)
   real(8),intent(in) :: expnt
   real(8) :: res,anres
   integer :: i
   real(8) :: x
   anres=sqrt(pi/(32.*expnt*expnt*expnt))
   res=0
   do i=1,1000;x=(i-500)*0.015
    res=res+0.015*(x*x*exp(-2.*expnt*x*x))
   enddo
   write(*,*) "INTCHECK : ",expnt,anres,res
   return
  end subroutine intcheck

  real(8) function intnum(pr)
   type(PRIM),intent(in) :: pr
   integer :: i,j,k,u,pow
   real(8) :: x,y,z,val,r2,r(3)
   intnum=0.
   do i=1,400;r(1)=(i-200)*0.04
   do j=1,400;r(2)=(j-200)*0.04
   do k=1,400;r(3)=(k-200)*0.04
    r2=r(1)**2+r(2)**2+r(3)**2
    val=exp(-1.*pr%expnt*r2)
    pow=1
    do u=1,3;if (pr%pow(u).gt.0) pow=pow*r(u);enddo
    val=val*pow*pr%coeff;val=val*val*(0.04*0.04*0.04)
    intnum=intnum+val
   enddo;enddo;enddo
   return
  end function intnum


  integer function nfromline(ctarg,nunit)
   integer,intent(in)::nunit
   character,intent(in) :: ctarg*20
   character :: cl*70,cs*20,ce*20 !cline,cend
   integer :: io,irew
   irew=0
   cs="flurp"
   do while (cs .ne. ctarg)
    read(nunit,'(A)',iostat=io) cl;cs=cl(1:20);ce=cl(55:)
    if (io.lt.0) then
     rewind(nunit)
     irew=irew+1
    endif
    if (irew.gt.1) then
     write(*,*) "Can't find label ",ctarg," on file. ";stop
    endif
   enddo
   read(ce,*) nfromline
   return
  end function nfromline

  subroutine printprim(pr)
   type(PRIM),intent(in) :: pr
   write(*,*) "PRIMITIVE : "
   write(*,*) "EXPONENT : ",pr%expnt
   write(*,*) "COEFF : ",pr%coeff
   write(*,*) "CART. POWERS : ",pr%pow
   write(*,*) "CENTRE : ",pr%xyz

   return
  end subroutine printprim



  real(8) function pint1D_rec(expnt,pow,x)
! this is a recurrence-relation integration function
! integrates {the function} x^pow * e^(-expnt*x*x) from -infinity to {the input variable} x 
! uses recurrence relation INTGL[x^i * gaussian] =   (i-1)/(2*expnt) * INTGL[x^(i-2)*gaussian)
!                                                  -     1/(2*expnt) *       x^(i-1)*gaussian
   implicit none
   real(8),intent(in) :: expnt,x
   integer,intent(in) :: pow
   logical :: aEVEN
   integer :: i
   real(8) :: gs
   gs=exp(-1.*expnt*x*x)     ! basic gaussian
   aEVEN=(mod(pow,2).eq.0)   ! is power even or not?
   if (aEVEN) then;
    pint1D_rec=(sqrt(pi/expnt))*0.5*(1.+erf(x*sqrt(expnt))) ! integral of x^0 * gs
   else;
    pint1D_rec=(-0.5/expnt)*gs                              ! integral of x^1 * gs
   endif
   do i=2+mod(pow,2),pow,2 ! now go up to x^pow * gs in steps of 2...
                           ! each iteration, pint1D_rec = integral of x^(i-2)*gs
                           ! use recurrence relation to calculate integral of x^i*gs
    pint1D_rec= (pint1D_rec*(i-1)/(2.*expnt) ) - (0.5/expnt)*(x**(i-1))*gs
   enddo
   return
  end function pint1D_rec
 
  function dint1D_rArr(expnt,pow) result (c)
! definite integral version of pint1D_rArr, returning array of all powers
   implicit none
   real(8),intent(in) :: expnt
   integer,intent(in) :: pow
   real(8) :: c(0:10)
   integer :: i
   real(8) :: gs
   c=0.
   do i=0,pow
    if(i.eq.0) then; c(i)=(sqrt(pi/expnt))
    elseif(mod(i,2).eq.1) then;c(i)=0.
    else; c(i)=(c(i-2)*(i-1)/(2.*expnt) ) 
    endif
   enddo 
   return
  end function dint1D_rArr

  function pint1D_rArr(expnt,pow,x) result (c)
! version of pint1D_rec that returns array of all powers up to specified power
   implicit none
   real(8),intent(in) :: expnt,x
   integer,intent(in) :: pow
   real(8) :: c(0:10)
   integer :: i
   real(8) :: gs
   c=0.
   gs=exp(-1.*expnt*x*x)     ! basic gaussian
!   if (pow.lt.10) c((pow+1):10)=0.  ! premature optimisation...
   do i=0,pow
    if(i.eq.0) then; c(i)=(sqrt(pi/expnt))*0.5*(1.+erf(x*sqrt(expnt)))
    elseif(i.eq.1) then;c(i)=(-0.5/expnt)*gs
    else; c(i)=(c(i-2)*(i-1)/(2.*expnt) ) - (0.5/expnt)*(x**(i-1))*gs
    endif
   enddo 
   return
  end function pint1D_rArr


  real(8) function pint1D_v2(expnt,pow,x)
   implicit none
   real(8),intent(in) :: expnt,x
   integer,intent(in) :: pow
   real(8) :: gs
   gs=exp(-1.*expnt*x*x)
   if (pow.eq.0) then
    pint1D_v2=(sqrt(pi/expnt))*0.5*(1.+erf(x*sqrt(expnt)))
   elseif (pow.eq.1) then
    pint1D_v2=(-0.5/expnt)*exp(-1.*expnt*x*x)
   elseif ((pow.eq.2).or.(pow.eq.4)) then
    pint1D_v2=(sqrt(pi/expnt))*0.5*(1.+erf(x*sqrt(expnt)))/(2.*expnt)
    pint1D_v2=pint1D_v2-x*exp(-1.*expnt*x*x)/(2.*expnt)
   elseif (pow.eq.3) then
    pint1D_v2=(-0.5/expnt)*exp(-1.*expnt*x*x)
    pint1D_v2=pint1D_v2*(x*x+(1./expnt))
   endif
   if (pow.eq.4) then
    pint1D_v2=pint1D_v2*(1.5/expnt)-(x*x*x)*exp(-1.*expnt*x*x)/(2.*expnt)
   endif
!   elseif (pow.eq.4) then
!    pint1D_v2=(sqrt(pi/expnt))*0.5*(1.+erf(x*sqrt(expnt)))/(2.*expnt)
!    pint1D_v2=pint1D_v2-x*exp(-1.*expnt*x*x)/(2.*expnt)  * 1.5
!    pint1D_v2=pint1D_v2*(x*x*x/(2.*expnt))*exp(-1.*expnt*x*x)
!   endif

   return
  end

  subroutine PRIMDUMP(wfn)
   type(WAVEFN),intent(in) :: wfn
   integer :: i,k
   open(unit=87,file="dump_prims",status="replace")
   write(87,*) " Coeff:   Exp :   Coords (x,y,z):         X^I Y^J Z^K: "
   write(*,*) "Dumping prims: number is ",wfn%nprim
   do i=1,wfn%nprim
    write(*,*) i
    write(87,'(5F9.4,3I4)') wfn%p(i)%coeff,wfn%p(i)%expnt,(wfn%p(i)%xyz(k),k=1,3),(wfn%p(i)%pow(k),k=1,3)
   enddo
   close(87)
   return
  end subroutine PRIMDUMP

end module PRIMS
