module OPTIONS
 use PRIMS
 implicit none

 integer,parameter :: nslots=3

 type GRID
  integer(8) :: npts
  integer(8) :: npt_u(3)
  real(8) :: org(3)
  real(8) :: du(3)
 end type GRID

 type(WAVEFN) :: wvfn(nslots)
 logical      :: awvfn(nslots)
 type(GRID)   :: wvgrd(nslots) 

 contains

  integer(8) function intprod(v)
   implicit none 
   integer(8),intent(in) :: v(3)
   intprod=(1+v(1))*(1+v(2))*(1+v(3))
   return
  end function intprod 

! Subroutine INIT: initialises stuff
  subroutine INIT()
   implicit none
   type(GRID) :: basgr
   awvfn=.FALSE.
   wvfn(:)%nomen = " empty slot "
   write(*,*) "Electronic structure manipulation stuff."
   write(*,*) "Takes in formatted checkpoint files."
   write(*,*) "Current status: "
   write(*,*) "-- Read in FCHK, construct n-space:     DONE"
   write(*,*) "-- Transfer n-functions between wvfns:  DONE"
   write(*,*) "-- Basis types:       S P SP      :     DONE"
   write(*,*) "                D Dcart F G       :     NOT DONE"
   write(*,*) "-- RI fitting:                          NOT DONE"

!   basgr%npt_u=(/150,150,150/)!;basgr%npts=11*11*801
!   basgr%npts=intprod(basgr%npt_u)
!   basgr%org=(/-4.5,-4.5,-4.5/)
!   basgr%du=(/0.06,0.06,0.06/)

   basgr%npt_u=(/40,40,40/)!;basgr%npts=11*11*801
   basgr%npts=intprod(basgr%npt_u)
   basgr%org=(/-2.0,-2.0,-2.0/)
   basgr%du=(/0.10,0.10,0.10/)

!   basgr%npt_u=(/0,0,60/);basgr%npts=101*101*101
!   basgr%org=(/-0.0,-0.0,-2.6/)
!   basgr%du=(/0.1,0.1,0.1/)
!   basgr%npt_u=(/16,16,16/);basgr%npts=17*17*17
!   basgr%org=(/-4.0,-4.0,-4.0/)
!   basgr%du=(/0.5,0.5,0.5/)
   wvgrd=basgr

   return
  end subroutine INIT

  subroutine MENU ()
   implicit none
   integer :: i,j,k
   integer :: invpow,kv(3)
   logical :: aCont,aRange
   character :: cinp*1,clong*30,cinp2*1
   type(GRID) :: basgr
   real(8) :: nl(3),rout(3),x,y,z,spectol,t_1,t_2
   aCont=.TRUE.
   do while (aCont)
    do i=1,nslots; call disp_wv(i); enddo
    write(*,*) "Enter option. [R] to READ a formatted checkpoint file."
    write(*,*) "A file, once read, can be [I]nverted."
    write(*,*) "Look up an n_r value with [L]."
    write(*,*) "Transfer MOs from wavefunction to wavefunction using [T]."
    write(*,*) "Calculate and dump MOs using [M]."
    write(*,*) "Generate orbitals using [P]lanewaves."
    write(*,*) "Modify grid parameters and reinitiali[S]e"
    write(*,*) "Once finished, you may [Q]uit."
!    write(*,*) "[C]heck: runs density check."
    read(*,*) cinp

    select case (cinp) 
     case ("Q","q"); stop 
     case ("S","s");
      basgr=wvgrd(1)
      write(*,*) "Select which one to change: "
      write(*,*) "[G]ridpoints : ",basgr%npt_u(:)
      write(*,*) "[O]rigin     : ",basgr%org(:)
      write(*,*) "[S]tepsize   : ",basgr%du(:)
      write(*,*) "[R]eturn "
      cinp2="D"; do while (cinp2.ne."R")
       read(*,*) cinp2
       select case(cinp2); case("G","g") ; read(*,*) (basgr%npt_u(k),k=1,3)
                            basgr%npts=intprod(basgr%npt_u)
                           case("O","o") ; read(*,*) (basgr%org(k),k=1,3)
                           case("S","s") ; read(*,*) (basgr%du(k),k=1,3)
       end select; enddo
       wvgrd=basgr

     case ("R","r"); write(*,*) "Enter name of formatted checkpoint file."; read(*,*) clong
      write(*,*) "Which slot should this go in?";read(*,*) i
      call cpu_time(t_1)
      call readformchk (clong,wvfn(i)); awvfn(i)=.TRUE.
      call cpu_time(t_2)
      write(*,*) "Total time for readformchk : ",(t_2-t_1)
!      write(*,*) "Calling calc_dens : "
!      call cpu_time(t_1)
!      call calc_dens(i)
!      call cpu_time(t_2)
!      write(*,*) "Density, bosonic orbital calculated. "
!      write(*,*) "Total time for calc_dens: ",(t_2-t_1)
      do  j=1,wvfn(i)%nat
       write(*,*) "Atom ",j, " atomic number ",wvfn(i)%zat(j)
       write(*,'(A10,3F9.4)') "RCoords: ",(wvfn(i)%xyz(j,k),k=1,3)
       x=wvfn(i)%xyz(j,1); y=wvfn(i)%xyz(j,2); z=wvfn(i)%xyz(j,3);
       write(*,'(A10,3F9.4)') "NCoords: ",&
         nint_xyz_FULL(wvfn(i),x,y,z)/nint_xyz_FULL(wvfn(i),1000000._8,y,z),&
         nint_yz_FULL(wvfn(i),y,z)/nint_yz_FULL(wvfn(i),1000000._8,z),&
         nint_z_FULL(wvfn(i),z)
      enddo

     case ("I","i"); write(*,*) "Enter slot to invert."; read(*,*) i
      if ( (i.gt.nslots).or.(i.lt.1)) then
       write(*,*) "Outside range of slots."
      elseif ( awvfn(i).eqv..false.) then
       write(*,*) "Nothing in slot."
      else
       write(*,*) "Enter tolerance."; read(*,*) spectol
!       write(*,*) "Enter number of points (will be rounded up to power of 2)."; read(*,*) invpow
!       spectol=0.001
       call SPECGRID_U(wvfn(i),spectol,-10._8,+10._8,3)
!       spectol=spectol*wvfn(i)%minseg(3)
       call SPECGRID_U(wvfn(i),spectol,-10._8,+10._8,2)
!       spectol=spectol*wvfn(i)%minseg(2)
       call SPECGRID_U(wvfn(i),spectol,-10._8,+10._8,1)
!       call INVERT_N(wvfn(i),invpow)
!       call MO_line(i)
      endif
!     case ("C","c");
     case ("L","l");write(*,*) "Enter slot to inquire of."; read(*,*) i
       write(*,*) "Enter 3 real values."; read(*,*) (nl(j),j=1,3)
       aRange=.true.
       do j=1,3;aRange=(aRange .and. (nl(j).ge.0.)) ; aRange=(aRange .and. (nl(j).le.1.0)); enddo
       if (aRange) then; call N_lookup(wvfn(I),nl,rout);
       else; write(*,*) "Range problem";endif
     case("T","t");write(*,*) "Enter slot to take MOs from: ";read(*,*)i
      write(*,*) "Enter slot to transfer MOs to: ";read(*,*) j
      call N_trans3(i,j)
!      call N_trans2(i,j)
     case("M","m");write(*,*) "Enter slot whose MOs to write to file: ";read(*,*)i
      call write_MOs(i)
     case("P","p");write(*,*) "Enter slot to generate MOs for : ";read(*,*)i
      write(*,*) "Enter k,l,m values : ";read(*,*) (kv(j),j=1,3)
      call calc_PW(i,kv)
     case default; write(*,*) "Option ",cinp," unrecognised or unimplemented."
    end select 
   enddo
   return
  end subroutine MENU

  subroutine MO_line(wv)
   implicit none
   integer,intent (in) :: wv
   integer :: i,j,k,l
   real(8),allocatable :: mos(:)
   character,allocatable :: c(:)*20 
   real(8) :: vec(3),cb,tot
   allocate(mos(wvfn(wv)%nmos))
   allocate(c(0:wvfn(wv)%nmos))
   do l=0,wvfn(wv)%nA
    write(c(l),'(A3,I1,A1,I1)') "LN_",wv,".",l
    open(unit=(10+l),file=c(l),status="replace")
   enddo
   vec=0.
   do i=0,wvfn(wv)%n_inv
    vec(3)=wvfn(wv)%az(i)
    call calc_MOs(wvfn(wv),vec,mos)
    cb=calcdmat(wvfn(wv),vec(1),vec(2),vec(3))
    cb=sqrt(cb)
    mos=mos/cb
    tot=0.
    do j=1,wvfn(wv)%nA
     write( (10+j),*) i,mos(j)
     tot=tot+mos(j)**2
    enddo
    write(10,*) i,tot
   enddo

   do l=0,wvfn(wv)%nA;close(10+l);enddo
   return
  end subroutine MO_line
  
  subroutine N_trans3 (w1,w2)
   implicit none
   integer,intent(in) :: w1
   integer,intent(out):: w2
   integer(4) :: ip1,ip2,ix,iy,iyz,iz,i,j,k,l,m,npt1(3),npt2(3),n12m,i0,j0,nspec1(3),ticker
   real(8),allocatable :: r12(:),r1(:),r2(:),rr1(:),rr2(:),zl(:),yzl(:,:)
   real(8),allocatable :: z12(:),yz12(:,:),xyz12(:,:,:),yz12b(:,:) ! coords for 2->1
   real(8),allocatable :: z2(:),y2(:),x2(:), z1(:),y1(:),x1(:) ! coords for 1, 2
   real(8),allocatable :: Rz_grid(:),Ry_grid(:)
   real(8),allocatable :: Ryz_grid(:,:),Rxyz_grid(:,:,:),Rscr_yz(:,:),Rscr_xyz(:,:)
   real(8) :: Ryz_max,Syz_max,Sxyz_max,Sx,Sy,Sz,dmax,u1,u2,upt
   integer(4),allocatable :: iz_grid(:),iyz_grid(:)
   integer(4),allocatable :: n12(:),itr_yz(:),itr_xyz(:)
   real(8) :: x,y,z,rscr,nt1,nt2,int1,int2,nx1,ny1,nz1,nx2,ny2,nz2,resvec(3),n2t,xlow,Nscr
   real(8) :: dx,dy,dz,d1,d2,d3,d4,d5
   real(8),allocatable :: mo12(:,:,:,:),moscr(:)
   real(8),allocatable :: cbos1(:,:,:)
   real(8),allocatable :: cbos2(:,:,:)
   real(8) :: cbos12
   character :: cf*9
   real(8) :: time1,time2,timetot,inttol,maxdev
   logical :: aDISP
!   real(8) :: intpol_z ! function
   inttol=0.00000001

   write(*,*) "MO-transference subroutine v. 3"
   call cpu_time(time1)
   npt1(:)=wvgrd(w1)%npt_u(:); npt2(:)=wvgrd(w2)%npt_u(:); nspec1(:)=wvfn(w1)%nspec(:)
   call Nt3_init()
   write(*,*) "Initialised stuff"
! set up R_z vector for target wavefunction.
   do k=0,npt2(3); Rz_grid(k)=nint_z_FULL(wvfn(w2),z2(k)); enddo
   iz=1;Nscr=wvfn(w1)%spec_u(3)%vec(iz,2)
   write(*,*) "Limits : ",npt2(3),nspec1(3)
   do k=0,npt2(3); ! write(*,*) k
    do while (Nscr.lt.Rz_grid(k))
     iz=iz+1;Nscr=wvfn(w1)%spec_u(3)%vec(iz,2); if (iz.eq.nspec1(3)) Nscr=1.01
                   enddo
    iz_grid(k)=iz
    if (iz.gt.1) then ! check whether higher iz is actually closest iz
     call intpolg_z(Rz_grid(k),z12(k),wvfn(w1)%spec_u(3)%vec(iz,1) )
    else; call intpolg_z(Rz_grid(k),z12(k),wvfn(w1)%spec_u(3)%vec(iz+1,1)  )
    endif
!    write(*,*) k,Rz_grid(k),nint_z_FULL(wvfn(w1),z12(k))
   enddo
   write(*,*) "Rz mapped out; npt2 : ",npt2(:)
   
! now that we have values for z for source system, we can set up an array of prim x prim values for z.
! note npts is for TARGET system (we have a list of points in source system
! which correspond to points in target)
   allocate(wvfn(w1)%pp_z(wvfn(w1)%nprim,wvfn(w1)%nprim,0:npt2(3)))
   do k=0,npt2(3); z=z12(k)
    do ip1=1,wvfn(w1)%nprim; do ip2=1,wvfn(w1)%nprim;
      wvfn(w1)%pp_z(ip1,ip2,k)=primval_1D(wvfn(w1)%p(ip1),z,3)*primval_1D(wvfn(w1)%p(ip2),z,3)
   enddo;enddo;enddo
! now do yz. Firstly, for min-z value, set up initial guess. 
   allocate(Rscr_yz(0:npt2(2),2));write(*,*) "Rscr_yz allocated"

! this is a very crude initial guess - it just makes an evenly-spaced grid.
   y=npt1(2)*wvgrd(w1)%du(2)
   Syz_max=nint_yz_FULL(wvfn(w1),1000._8,z12(0))
   do j=0,npt2(2)
    if (j.eq.0) then; Rscr_yz(j,1)=wvgrd(w1)%org(2)-3.
    elseif(j.eq.npt2(2)) then;  Rscr_yz(j,1)=wvgrd(w1)%org(2)+3.+y
    else; Rscr_yz(j,1)=wvgrd(w1)%org(2)+real(j)*y/real(npt2(2))
    endif
    Rscr_yz(j,2)=nint_yz_FULL(wvfn(w1),Rscr_yz(j,1),z12(0))/Syz_max
   enddo
! now loop over k
   inttol=inttol*10.
   yz_loop: do k=0,npt2(3)
! create vector of target grid
    do j=0,npt2(2);Ryz_grid(j,k)=nint_yz_FULL(wvfn(w2),y2(j),z2(k));enddo
    Ryz_grid(:,k)=Ryz_grid(:,k)/nint_yz_FULL(wvfn(w2),1000._8,z2(k))
    Syz_max=nint_yz_FULL(wvfn(w1),1000._8,z12(k))
! Rscr_yz is the trial vector. If k=0 the R values need calculating.
! If k>0, there is a previous vector that can be used (with stored R values)
    if (k.eq.0) then 
     do j=0,npt2(2);Rscr_yz(j,2)=nint_yz_OPT(wvfn(w1),Rscr_yz(j,1),k)/Syz_max;enddo
    endif
! now step through.
    iy=0;Nscr=Rscr_yz(0,2)
    do j=0,npt2(2)!;write(*,*) j!;write(*,*) j," looking for ",Ryz_grid(j,k)," current :",Nscr
     do while (Nscr.lt.Ryz_grid(j,k))
       iy=iy+1;Nscr=Rscr_yz(iy,2)
       if (iy.eq.npt2(2)) Nscr=1.01
     enddo
     call intpolg_y(Ryz_grid(j,k),Rscr_yz(iy,1),yz12(j,k),z12(k),k)
    enddo
    Rscr_yz(:,1)=yz12(:,k)
    Rscr_yz(:,2)=Ryz_grid(:,k)
   enddo yz_loop

!   do j=0,npt2(2);do k=0,npt2(3);z=z12(k);y=yz12(j,k)
!    d1=nint_yz_FULL(wvfn(w1),y,z)
!    d2=nint_yz_OPT(wvfn(w1),y,k)
!    write(*,*) j,k,d2-d1
!   enddo;enddo
   yz12b=yz12
! now that we have values for y, z for source system, we can set up prim x prim values for y.
   allocate(wvfn(w1)%pp_yz(wvfn(w1)%nprim,wvfn(w1)%nprim,0:npt2(2),0:npt2(3)))
   allocate(wvfn(w2)%pp_yz(wvfn(w2)%nprim,wvfn(w2)%nprim,0:npt2(2),0:npt2(3)))
   open(unit=55,file="xyzcheck_init",status="replace")
   do k=0,npt2(3);z=z12(k); do j=0,npt2(2);y=yz12(j,k)
    do ip1=1,wvfn(w1)%nprim; do ip2=1,wvfn(w1)%nprim;
      wvfn(w1)%pp_yz(ip1,ip2,j,k)=primval_1D(wvfn(w1)%p(ip1),y,2)*primval_1D(wvfn(w1)%p(ip2),y,2) &
               * primval_1D(wvfn(w1)%p(ip1),z,3)*primval_1D(wvfn(w1)%p(ip2),z,3)
    enddo;enddo
    do ip1=1,wvfn(w2)%nprim; do ip2=1,wvfn(w2)%nprim;
      wvfn(w2)%pp_yz(ip1,ip2,j,k)=primval_1D(wvfn(w2)%p(ip1),y2(j),2)*primval_1D(wvfn(w2)%p(ip2),y2(j),2) &
               * primval_1D(wvfn(w2)%p(ip1),z2(k),3)*primval_1D(wvfn(w2)%p(ip2),z2(k),3)
    enddo;enddo
    d1=nint_xyz_FULL(wvfn(w1),0._8,y,z)
    d2=nint_xyz_OPT(wvfn(w1),0._8,j,k)
    d3=d1-d2
    d1=nint_xyz_FULL(wvfn(w1),-2._8,y,z)
    d2=nint_xyz_OPT(wvfn(w1),-2._8,j,k)
    d4=d1-d2
    d1=nint_xyz_FULL(wvfn(w1),2._8,y,z)
    d2=nint_xyz_OPT(wvfn(w1),2._8,j,k)
    d5=d1-d2
    if (max(abs(d5),abs(d4),abs(d3)).gt.0.000001) write(55,'(2I5,3F12.7)') j,k,d3,d4,d5
   enddo;enddo
   close(55)
!   open(unit=54,file="xyz28_0",status="replace");z=z12(0);y=yz12(28,0)
!   write(*,*) 28,0,y,z
!   do ip1=1,wvfn(w1)%nprim; do ip2=1,wvfn(w1)%nprim;
!    write(54,*) ip1,ip2,wvfn(w1)%pp_yz(ip1,ip2,28,0),&
!     primval_1D(wvfn(w1)%p(ip1),y,2)*primval_1D(wvfn(w1)%p(ip2),y,2) &
!               * primval_1D(wvfn(w1)%p(ip1),z,3)*primval_1D(wvfn(w1)%p(ip2),z,3)
!   enddo;enddo
!   close(54)

! now do xyz. Firstly, for min-xyz value, set up initial guess. 
   allocate(Rscr_xyz(0:npt2(2),2));write(*,*) "Rscr_xyz allocated"
! same very crude initial guess as for yz
   x=npt1(1)*wvgrd(w1)%du(1)
!   write(*,*) "x range for source : ",x
   Sxyz_max=nint_xyz_FULL(wvfn(w1),1000._8,yz12(0,0),z12(0))
   do i=0,npt2(1)
    if (i.eq.0) then; Rscr_xyz(i,1)=wvgrd(w1)%org(1)-3.
    elseif(i.eq.npt2(1)) then;  Rscr_xyz(i,1)=wvgrd(w1)%org(1)+3.+x
    else; Rscr_xyz(i,1)=wvgrd(w1)%org(1)+real(i)*x/real(npt2(1))
    endif
    Rscr_xyz(i,2)=nint_xyz_FULL(wvfn(w1),Rscr_xyz(i,1),yz12(0,0),z12(0))/Sxyz_max
!    write(*,*) "Guess : ",i,Rscr_xyz(i,:),x,wvgrd(w1)%org(1)
   enddo

   write(*,*) "Initial guess set up"
! loop over y,z
   inttol=inttol*10.
   xyz_loop: do k=0,npt2(3);z=z12(k);do j=0,npt2(2) ; y=yz12(j,k)
!   xyz_loop: do k=17,17;z=z12(k);write(*,*) k,z;do j=26,26 ; y=yz12(j,k)
!     write(*,*) "XYZ loop: ",j,k,yz12(j,k),z12(k),y,z
! calc vec for target system, these are values that need to be found in source system
!    do i=0,npt2(1);Rxyz_grid(i,j,k)=nint_xyz_FULL(wvfn(w2),x2(i),y2(j),z2(k));enddo
!    aDISP=( (j.eq.26).and.(k.eq.14))

     do i=0,npt2(1);Rxyz_grid(i,j,k)=nint_xyz_OPT(wvfn(w2),x2(i),j,k);
!      d1=abs(nint_xyz_OPT(wvfn(w2),x2(i),j,k)-nint_xyz_FULL(wvfn(w2),x2(i),y2(j),z2(k))) ! DEBUG
!      if (d1.gt.0.000001) write(*,*) "WARNING: target : OPT v. FULL dev ",d1             ! DEBUG
     enddo
     Rxyz_grid(:,j,k)=Rxyz_grid(:,j,k)/nint_xyz_FULL(wvfn(w2),1000._8,y2(j),z2(k)) ! /Sxyz_max for TARGET 
     Sxyz_max=nint_xyz_FULL(wvfn(w1),1000._8,y,z)
! redo calc of initial guess (needed when skipping j,k for debugging)
!     do i=0,npt2(1);Rscr_xyz(i,2)=nint_xyz_FULL(wvfn(w1),Rscr_xyz(i,1),y,z)/Sxyz_max;enddo
!     write(*,*) " i    Targ-x    Targ-Rx    prov Src-x   Src-Rx"
!     do i=0,npt2(1);write(*,'(I4,4F10.5)') i,x2(i),Rxyz_grid(i,j,k),Rscr_xyz(i,1),Rscr_xyz(i,2);enddo
!    write(*,*) "Calculated target vec " 
!    do i=0,npt2(1); write(*,'(I4,2F9.4,4X,2F9.4)') i,x2(i),Rxyz_grid(i,j,k),Rscr_xyz(i,:);enddo

! init guess: should be carried over from previous iteration; unless j=0,k=0,
! in which case calculations of crude initial guess above are still in Rscr_xyz
! loop over initguess vec to find closest
     ix=0;Nscr=Rscr_xyz(0,2)
     x_loop: do i=0,npt2(1)!;write(*,*) i,j,k,yz12(j,k),z12(k)
      do while (Nscr.lt.Rxyz_grid(i,j,k))
        ix=ix+1;Nscr=Rscr_xyz(ix,2)
        if (ix.eq.npt2(1)) Nscr=1.01
      enddo
      if (ix.gt.npt2(1)) then;write(*,*) "ix too high : ",ix,npt2(1);stop;endif
!      write(*,*) i,Rxyz_grid(i,j,k),"   ",ix,Rscr_xyz(ix,2)
      rscr=Rscr_xyz(ix,1)
!      if( (ix.gt.1).and.(ix.lt.npt2(1))) then ! interpolate once
!       rscr=(Rxyz_grid(i,j,k) - Rscr_xyz(ix-1,2)) / (Rscr_xyz(ix,2)-Rscr_xyz(ix-1,2))
!       rscr=rscr*(Rscr_xyz(ix,1)-Rscr_xyz(ix-1,1)) + Rscr_xyz(ix-1,1)
!      endif
!      write(*,*) i,Rxyz_grid(i,j,k),"   ",ix,Rscr_xyz(ix,2),nint_xyz_FULL(wvfn(w1),rscr,y,z)/Sxyz_max
      call intpolg_x(Rxyz_grid(i,j,k),rscr,xyz12(i,j,k),y,z,j,k,.false.) !;endif
!     if (aDISP) then; write(*,*) "Calling intpolg_x"
!     call intpolg_x(Rxyz_grid(i,j,k),Rscr_xyz(ix,1),xyz12(i,j,k),y,z,i,j,.true.);else
!      call intpolg_x(Rxyz_grid(i,j,k),Rscr_xyz(ix,1),xyz12(i,j,k),y,z,i,j,.false.) !;endif
!      write(*,*) i,Rxyz_grid(i,j,k),"   ",ix,Rscr_xyz(ix,2),nint_xyz_FULL(wvfn(w1),xyz12(i,j,k),y,z)/Sxyz_max
!      write(*,*) "Called intpolg_x"
!      d1=nint_xyz_FULL(wvfn(w1),xyz12(i,j,k),y,z)/Sxyz_max
!      d2=nint_xyz_OPT(wvfn(w1),xyz12(i,j,k),j,k)/Sxyz_max
!      if (abs(d2-d1).gt.0.00000001) write(*,*) "ERROR (opt Rxyz) ! :",d1,d2,abs(d2-d1)
!      d2=nint_xyz_FULL(wvfn(w2),x2(i),y2(j),z2(k))/nint_xyz_FULL(wvfn(w2),9000._8,y2(j),z2(k))
!      if (abs(d2-d1).gt.0.00001) write(*,*) "ERROR (fit Rxyz) ! :",d1,d2,abs(d2-d1)
     enddo x_loop
     if (j.lt.npt2(2)) then 
      Rscr_xyz(:,1)=xyz12(:,j,k)
      Rscr_xyz(:,2)=Rxyz_grid(:,j,k)
     else
      Rscr_xyz(:,1)=xyz12(:,0,k)
      Rscr_xyz(:,2)=Rxyz_grid(:,0,k)
     endif
!     write(*,*) "   Check: ",nint_xyz_FULL(wvfn(w1),0._8,y,z),nint_xyz_OPT(wvfn(w1),0._8,j,k)
!     write(*,*) "XYZ loop: ",j,k,yz12(j,k),z12(k),y,z
   enddo;enddo xyz_loop
!   d3=0.
!   do i=0,npt2(1);do j=0,npt2(2);do k=0,npt2(3);z=z2(k);y=y2(j);x=x2(i)
!    d1=nint_xyz_FULL(wvfn(w2),x,y,z)
!    d2=nint_xyz_OPT(wvfn(w2),x,j,k)
!    d3=max(d3,abs(d1-d2))
!    write(*,*) i,j,k,d1,d2,d2-d1
!   enddo;enddo;enddo
!   write(*,*) "Max error : ",d3
   call cpu_time(time2)
   write(*,*) "Time for xyz, yz and z mapping: ",time2-time1

   call cpu_time(time1)
! now check
   maxdev=0.
   do k=0,npt2(3); dz=( nint_z_FULL(wvfn(w2),z2(k))-nint_z_FULL(wvfn(w1),z12(k)))**2
    Syz_max=nint_yz_FULL(wvfn(w1),1000._8,z12(k))
    do j=0,npt2(2); 
     dy=nint_yz_FULL(wvfn(w1),yz12(j,k),z12(k))/Syz_max
     dy=dy-( nint_yz_FULL(wvfn(w2),y2(j),z2(k))/nint_yz_FULL(wvfn(w2),1000._8,z2(k)) )
     dy=dy**2
     Sxyz_max=nint_xyz_FULL(wvfn(w1),1000._8,yz12(j,k),z12(k))
     do i=0,npt2(1)
      dx=nint_xyz_FULL(wvfn(w1),xyz12(i,j,k),yz12(j,k),z12(k))/Sxyz_max
      dx=dx-( nint_xyz_FULL(wvfn(w2),x2(i),y2(j),z2(k))/nint_xyz_FULL(wvfn(w2),1000._8,y2(j),z2(k)) )
      dx=dx**2
!      if (sqrt(dx+dy+dz).gt.0.00001) then
!       write(*,*) i,j,k,dx,dy,dz,sqrt(dx+dy+dz)
!      endif
      maxdev=max(maxdev,sqrt(dx+dy+dz))
   enddo;enddo;enddo
   write(*,*) "Checked. Greatest deviation : ",maxdev
   call cpu_time(time2)
   write(*,*) "Time for high-accuracy deviation check : ",time2-time1

   call cpu_time(time1)
! now check
   maxdev=0.
   do k=0,npt2(3); dz=( nint_z_FULL(wvfn(w2),z2(k))-nint_z_FULL(wvfn(w1),z12(k)))**2
    Syz_max=nint_yz_FULL(wvfn(w1),1000._8,z12(k))
    do j=0,npt2(2); 
     dy=nint_yz_FULL(wvfn(w1),yz12(j,k),z12(k))/Syz_max
     dy=dy-( nint_yz_FULL(wvfn(w2),y2(j),z2(k))/nint_yz_FULL(wvfn(w2),1000._8,z2(k)) )
     dy=dy**2
     Sxyz_max=nint_xyz_OPT(wvfn(w1),1000._8,j,k)
     do i=0,npt2(1)
      dx=nint_xyz_OPT(wvfn(w1),xyz12(i,j,k),j,k)/Sxyz_max
      dx=dx-( nint_xyz_OPT(wvfn(w2),x2(i),j,k)/nint_xyz_OPT(wvfn(w2),1000._8,j,k ))
      dx=dx**2
!      if (sqrt(dx+dy+dz).gt.0.00001) then
!       write(*,*) i,j,k,dx,dy,dz,sqrt(dx+dy+dz)
!      endif
      maxdev=max(maxdev,sqrt(dx+dy+dz))
   enddo;enddo;enddo
   write(*,*) "Checked. Greatest deviation : ",maxdev
   call cpu_time(time2)
   write(*,*) "Time for optimised deviation check : ",time2-time1


! now set up cbos

   do k=0,npt2(3); do j=0,npt2(2); do i=0,npt2(1)
      cbos1(i,j,k)=sqrt( calcdmat(wvfn(w1),xyz12(i,j,k),yz12(j,k),z12(k)) / wvfn(w1)%nA)
      cbos2(i,j,k)=sqrt( calcdmat(wvfn(w2),x2(i),y2(j),z2(k)) / wvfn(w2)%nA)
      cbos12=cbos2(i,j,k)/cbos1(i,j,k)
      resvec=(/xyz12(i,j,k),yz12(j,k),z12(k)/)
      call calc_MOs(wvfn(w1),resvec,moscr)
      mo12(:,i,j,k)=cbos12*moscr(:)
   enddo;enddo;enddo

!   do m=1,wvfn(w1)%nA
!    do k=0,npt2(3); do j=0,npt2(2); do i=0,npt2(1)
!     mo(i,j,k)=wvfn
!    enddo;enddo;enddo
!
!    write(cf,'(A3,I1,A5)') "MOt",m,".cube"
!    open(unit=15,file=cf,status="replace")
!    write(15,*) " flurp";write(15,*) " "
!    write(15,*) wvfn(w2)%nat,(wvgrd(w2)%org(k),k=1,3)
!    write(15,*) 1+wvgrd(w2)%npt_u(1),wvgrd(w2)%du(1),0.,0.
!    write(15,*) 1+wvgrd(w2)%npt_u(2),0.,wvgrd(w2)%du(2),0.
!    write(15,*) 1+wvgrd(w2)%npt_u(3),0.,0.,wvgrd(w2)%du(3)
!    do i=1,wvfn(w2)%nat;write(15,*) wvfn(w2)%zat(i),wvfn(w2)%zat(i)*1.,&
!       (wvfn(w2)%xyz(i,k),k=1,3);enddo
!    write(15,'(6F15.9)') ((( wvfn(w2)%mos(m,i,j,k),k=0,npt2(3)),j=0,npt2(2)),i=0,npt2(1))
!    close(15)
!    write(cf,'(A3,I1,A5)') "MOt",m,".line"
!    open(unit=16,file=cf,status="replace")
!     do k=0,npt2(3)
!      if (m.eq.2) then
!       write(16,*) (wvgrd(w2)%org(3)+k*wvgrd(w2)%du(3)), &
!          0.99924*wvfn(w2)%mos(2,i0,j0,k)+0.039*wvfn(w2)%mos(3,i0,j0,k)
!      elseif(m.eq.3) then
!       write(16,*) (wvgrd(w2)%org(3)+k*wvgrd(w2)%du(3)), &
!          0.99924*wvfn(w2)%mos(3,i0,j0,k)-0.039*wvfn(w2)%mos(2,i0,j0,k)
!      else
!       write(16,*) (wvgrd(w2)%org(3)+k*wvgrd(w2)%du(3)),wvfn(w2)%mos(m,i0,j0,k)
!      endif
!     enddo
!    close(16)
!   enddo


   call dealloc()
   return
   contains
   subroutine intpolg_x(Rtarg,xin,xout,yS,zS,jy,kz,aP)
    implicit none
    real(8),intent(in):: Rtarg,xin,yS,zS
    integer,intent(in):: jy,kz
    real(8),intent(out) :: xout
    logical, intent(in) :: aP
    integer :: iii,jjj
    real(8) :: x0,xnew,d1,d2,R0,Rnew,deriv,dR,dX,dXscl
    x0=xin
!    R0=nint_xyz_FULL(wvfn(w1),x0,yS,zS)/Sxyz_max 
    R0=nint_xyz_OPT(wvfn(w1),x0,jy,kz)/Sxyz_max 
!    if (aP) write(*,*) "R0 check: ",nint_xyz_OPT(wvfn(w1),x0,jy,kz),nint_xyz_FULL(wvfn(w1),x0,yS,zS),&
!     Sxyz_max
    d1=abs(R0-Rtarg)
!    if (aP) write(*,*) "Starting : ",xin,R0,"looking for : ",Rtarg,d1
!    if (d1.lt.inttol) then;xout=x0;return;endif
!    write(*,*) d1," too large "
    d2 = 10000.;iii=0
    do while (d1.gt.inttol)
     iii=iii+1
     deriv=dnint_xyz_OPT(wvfn(w1),x0,jy,kz)/Sxyz_max 
     dR=Rtarg-R0
     dX=dR/deriv
     xnew=x0+dX
     Rnew=nint_xyz_OPT(wvfn(w1),xnew,jy,kz)/Sxyz_max
!     Rnew=nint_xyz_FULL(wvfn(w1),xnew,yS,zS)/Sxyz_max 
     d2=abs(Rnew-Rtarg)
     dXscl=1.
 !    if (aP)  write(*,*) "Iteration ",iii,Rnew,Rtarg," diff ",d2
     jjj=0
     do while (d2.gt.d1) 
!      jjj=jjj+1
      dXscl=dXscl*0.4
      xnew=x0+dX*dXscl
      Rnew=nint_xyz_OPT(wvfn(w1),xnew,jy,kz)/Sxyz_max
!      Rnew=nint_xyz_FULL(wvfn(w1),xnew,yS,zS)/Sxyz_max 
      d2=abs(Rnew-Rtarg)
!      write(*,*) "Intraiterational iteration ",jjj,d1,d2
     enddo
     x0=xnew;R0=Rnew;d1=d2
     if (iii.gt.20) then;
      write(*,*) "Too many iterations",iii,xin,xnew,Rnew,Rtarg
      write(*,*)  nint_xyz_OPT(wvfn(w1),xnew,jy,kz)/Sxyz_max,nint_xyz_FULL(wvfn(w1),xnew,yS,zS)/Sxyz_max
      call nint_xyz_DEBUG(wvfn(w1),xnew,yS,zS,jy,kz)

     endif
     if (iii.gt.20) then;stop;endif
    enddo
    xout=x0
!    write(*,*) "Finished after ",iii," iterations "
    return
   end subroutine intpolg_x

   subroutine intpolg_y(Rtarg,xin,xout,zS,kz)
    implicit none
    real(8),intent(in):: Rtarg,xin,zS
    integer,intent(in):: kz
    real(8),intent(out) :: xout
    integer :: iii,jjj
    real(8) :: x0,xnew,d1,d2,R0,Rnew,deriv,dR,dX,dXscl
    x0=xin
    R0=nint_yz_OPT(wvfn(w1),x0,kz)/Syz_max 
    d1=abs(R0-Rtarg)
!    write(*,*) "Starting : ",xin,R0,"looking for : ",Rtarg,d1
    if (d1.lt.inttol) then;xout=x0;return;endif
!    write(*,*) d1," too large "
    d2 = 10000.;iii=0
    do while (d1.gt.inttol)
     iii=iii+1
     deriv=dnint_yz_FULL(wvfn(w1),x0,zS)/Syz_max 
     dR=Rtarg-R0
     dX=dR/deriv
     xnew=x0+dX
     Rnew=nint_yz_OPT(wvfn(w1),xnew,kz)/Syz_max
     d2=abs(Rnew-Rtarg)
     dXscl=1.
!     write(*,*) "Iteration ",iii,Rnew,Rtarg," diff ",d2
     jjj=0
     do while (d2.gt.d1) 
!      jjj=jjj+1
      dXscl=dXscl*0.4
      xnew=x0+dX*dXscl
      Rnew=nint_yz_OPT(wvfn(w1),xnew,kz)/Syz_max
      d2=abs(Rnew-Rtarg)
!      write(*,*) "Intraiterational iteration ",jjj,d1,d2
     enddo
     x0=xnew;R0=Rnew;d1=d2
    enddo
    xout=x0
!    write(*,*) "Finished after ",iii," iterations "
    return
   end subroutine intpolg_y

   subroutine intpol_y(Rtarg,iiy,zS,ans,aMan0,xMan0) 
    implicit none
    real(8),intent(out):: ans
    integer,intent(in) :: iiy
    real(8),intent(in) :: Rtarg,zS,xMan0
    logical,intent(in) :: aMan0
    real(8) :: x_0,x_1,x_m, R_0,R_1,R_m,x_d,R_d,R_rat
    real(8) :: d_0,d_1,d_m
    integer :: iii
    logical :: aH
    if (aMan0.and.(xMan0.gt.Rscr_yz(iiy-1,1))) then
     x_0=xMan0
     R_0=nint_yz_FULL(wvfn(w1),x_0,zS)/Syz_max
    else
     x_0=Rscr_yz(iiy-1,1)  
     R_0=Rscr_yz(iiy-1,2)  
    endif
    x_1=Rscr_yz(iiy,1)
    R_1=Rscr_yz(iiy,2)
    d_0=abs(Rtarg-R_0)    ; d_1=abs(Rtarg-R_1)
    if (d_0.lt.inttol) then;ans=x_0;return
    elseif(d_1.lt.inttol) then; ans=x_1;return
    endif
    d_m=1000.;iii=0;! aH=( (Rtarg.lt.0.01).or.(Rtarg.gt.0.99))
    do while (d_m.gt.inttol)!.and.iii.lt.10)
     iii=iii+1!;if(mod(iii,10).eq.0)write(*,*) iii
     R_d=R_1-R_0;R_m=Rtarg-R_0;R_rat=R_m/R_d
     if (mod(iii,2).eq.0) then; x_m=0.5*(x_0+x_1)
     else; x_m=x_0+R_rat*(x_1-x_0);endif
     R_m=nint_yz_FULL(wvfn(w1),x_m,zS)/Syz_max
     d_m=abs(R_m-Rtarg)
     if (j.eq.9) write(*,'(I4,7F9.5)') iii,d_m,x_0,x_m,x_1,R_0,R_m,R_1
     if (d_m.lt.inttol) then;
      ans=x_m !;return
     elseif (R_m.lt.Rtarg) then;
       x_0=x_m;R_0=R_m
     else; x_1=x_m;R_1=R_m; endif
     if (iii.gt.20) then;
      write(*,*) "iterations too high"; stop
     elseif(iii.gt.6) then;
      R_0=nint_yz_FULL(wvfn(w1),x_0,zS)/Syz_max
      R_1=nint_yz_FULL(wvfn(w1),x_1,zS)/Syz_max
     endif
    enddo
    return 
   end subroutine intpol_y
   subroutine  intpolg_z(Rtarg,zout,zin)
    implicit none
    real(8),intent(in) :: Rtarg,zin
    real(8),intent(out) :: zout
    real(8) :: znew,zold,Rnew,Rold,grad,Rdev,Rdold,ddz  
    integer :: iii
    znew=zin
    Rnew=nint_z_FULL(wvfn(w1),znew)
    grad=dnint_z_FULL(wvfn(w1),znew)
    Rdev=Rtarg-Rnew
    iii=0
!    write(*,*) "Initiating intpolg_z. ",znew,Rtarg,Rnew
    do while (abs(Rdev).gt.inttol)
! need to move R by Rdev:  dR/dz ~= grad; dz=dR/grad=Rdev/grad
     iii=iii+1
     ddz=Rdev/grad
     zold=znew;Rold=Rnew;Rdold=Rdev
!     write(*,*) iii,znew,Rnew,Rdev,grad,ddz
!     write(*,*) "Iteration : ",iii," deviation : ",Rdev
     znew=zold+ddz
     Rnew=nint_z_FULL(wvfn(w1),znew)
     Rdev=Rtarg-Rnew
!     write(*,*) "Modification: dev now ",Rdev
     do while (abs(Rdev).gt.abs(Rdold))
      
      ddz=0.5*ddz
      znew=zold+ddz
      Rnew=nint_z_FULL(wvfn(w1),znew)
      Rdev=Rtarg-Rnew
     enddo
!     write(*,*) abs(Rdev)," now less than ",abs(Rdold)
     grad=dnint_z_FULL(wvfn(w1),znew)
     if (iii.gt.500) then;write(*,*) "Stopping intpolg_z after ",iii," iterations.";stop;endif
    enddo 
!    write(*,*) "Converged after ",iii," iterations"
    zout=znew
    return
   end subroutine intpolg_z

   real(8) function intpol_z(Rtarg,iiz)
    implicit none
    integer,intent(in) :: iiz
    real(8),intent(in) :: Rtarg
    real(8) :: x_0,x_1,x_m, R_0,R_1,R_m,x_d,R_d,R_rat
    real(8) :: d_0,d_1,d_m
    integer :: iii
    x_0=wvfn(w1)%spec_u(3)%vec(iiz-1,1) ; x_1=wvfn(w1)%spec_u(3)%vec(iiz,1)
    R_0=wvfn(w1)%spec_u(3)%vec(iiz-1,2) ; R_1=wvfn(w1)%spec_u(3)%vec(iiz,2)
    d_0=abs(Rtarg-R_0)                 ; d_1=abs(Rtarg-R_1)
    if (d_0.lt.inttol) then;intpol_z=R_0;return
    elseif(d_1.lt.inttol) then; intpol_z=R_1;return
    endif
    d_m=1000.
    iii=0
    do while (d_m.gt.inttol)
     iii=iii+1;write(*,*) "In intpol_y; iii : ",iii
     R_d=R_1-R_0;R_m=Rtarg-R_0;R_rat=R_m/R_d
     x_m=x_0+R_rat*(x_1-x_0)
     R_m=nint_z_FULL(wvfn(w1),x_m)
     d_m=abs(R_m-Rtarg)
     if (d_m.lt.inttol) then; intpol_z=R_m;return
     elseif (R_m.lt.Rtarg) then; x_0=x_m;R_0=R_m
     else; x_1=x_m;R_1=R_m; endif
    enddo
    return 
   end function intpol_z
   subroutine dealloc()
    implicit none
    deallocate(x2); deallocate(y2); deallocate(z2)
    deallocate (yz12b);deallocate(Rz_grid);deallocate(Ry_grid);deallocate(iz_grid)
    deallocate (iyz_grid);deallocate(moscr);deallocate(mo12);deallocate(cbos1);deallocate(cbos2)
    deallocate (Rscr_yz); deallocate (Rscr_xyz)
    return
   end subroutine dealloc
   subroutine Nt3_init()
    implicit none
    integer :: kk
! firstly, set up coord. lookup for w2 to avoid calculating it repeatedly.
    allocate (x2(0:wvgrd(w2)%npt_u(1))); allocate (y2(0:wvgrd(w2)%npt_u(2))); allocate (z2(0:wvgrd(w2)%npt_u(3)))
    do i=0,wvgrd(w2)%npt_u(1);x2(i)=wvgrd(w2)%org(1)+i*wvgrd(w2)%du(1);enddo
    do j=0,wvgrd(w2)%npt_u(2);y2(j)=wvgrd(w2)%org(2)+j*wvgrd(w2)%du(2);enddo
    do k=0,wvgrd(w2)%npt_u(3);z2(k)=wvgrd(w2)%org(3)+k*wvgrd(w2)%du(3);enddo
! now allocate z12,yz12,xyz12, for storing the results.
    allocate (z12(0:wvgrd(w2)%npt_u(3)))
!    allocate (y12(0:wvgrd(w2)%npt_u(2)))
    allocate (yz12(0:wvgrd(w2)%npt_u(2) , 0:wvgrd(w2)%npt_u(3)))
    allocate (yz12b(0:wvgrd(w2)%npt_u(2) , 0:wvgrd(w2)%npt_u(3)))
    allocate (xyz12(0:wvgrd(w2)%npt_u(1) , 0:wvgrd(w2)%npt_u(2) , 0:wvgrd(w2)%npt_u(3)))
! allocate nz_grid
    allocate (Rz_grid(0:npt2(3)))
    allocate (Ry_grid(0:npt2(2)))
    allocate (iz_grid(0:npt2(3)))
    allocate (Ryz_grid(0:npt2(2),0:npt2(3)))
    allocate (Rxyz_grid(0:npt2(1),0:npt2(2),0:npt2(3)))
    allocate (iyz_grid(0:npt2(2)))
    allocate (moscr(wvfn(w1)%nA))
    allocate (mo12 (wvfn(w1)%nA,0:npt1(1),0:npt1(2),0:npt1(3)))
    allocate (cbos1(0:npt1(1),0:npt1(2),0:npt1(3)))
    allocate (cbos2(0:npt2(1),0:npt2(2),0:npt2(3)))

    return
   end subroutine Nt3_init
  end subroutine N_trans3

  subroutine N_trans2 (w1,w2)
   implicit none
   integer,intent(in) :: w1
   integer,intent(out):: w2
   integer(4) :: i,j,k,l,m,npt1(3),npt2(3),n12m,i0,j0
   real(8),allocatable :: r12(:),r1(:),r2(:),rr1(:),rr2(:),zl(:),yzl(:,:)
   real(8),allocatable :: z12(:),yz12(:,:),xyz12(:,:,:) ! coords for 2->1
   real(8),allocatable :: z2(:),y2(:),x2(:), z1(:),y1(:),x1(:) ! coords for 1, 2
   integer(4),allocatable :: n12(:)
   real(8) :: x,y,z,nt1,nt2,int1,int2,nx1,ny1,nz1,nx2,ny2,nz2,resvec(3),n2t,cbos,cbos2,xlow
   real(8),allocatable :: mos(:)
   character :: cf*9
   real(8) :: time1,time2,timetot

   call cpu_time(time1)
   npt1(:)=wvgrd(w1)%npt_u(:)
   npt2(:)=wvgrd(w2)%npt_u(:)
! firstly, set up coord. lookup for w2 and w1 to avoid calculating it repeatedly.
   allocate (x1(0:wvgrd(w1)%npt_u(1))); allocate (y1(0:wvgrd(w1)%npt_u(2))); allocate (z1(0:wvgrd(w1)%npt_u(3)))
   do i=0,wvgrd(w1)%npt_u(1);x1(i)=wvgrd(w1)%org(1)+i*wvgrd(w1)%du(1);enddo
   do j=0,wvgrd(w1)%npt_u(2);y1(j)=wvgrd(w1)%org(2)+j*wvgrd(w1)%du(2);enddo
   do k=0,wvgrd(w1)%npt_u(3);z1(k)=wvgrd(w1)%org(3)+k*wvgrd(w1)%du(3);enddo
   x1(0)=x1(0)-5.; y1(0)=y1(0)-5.; z1(0)=z1(0)-5.;
   x1(npt1(1))=x1(npt1(1))+5.; y1(npt1(2))=y1(npt1(2))+5.; z1(npt1(3))=z1(npt1(3))+5.;
   allocate (x2(0:wvgrd(w2)%npt_u(1))); allocate (y2(0:wvgrd(w2)%npt_u(2))); allocate (z2(0:wvgrd(w2)%npt_u(3)))
   do i=0,wvgrd(w2)%npt_u(1);x2(i)=wvgrd(w2)%org(1)+i*wvgrd(w2)%du(1);enddo
   do j=0,wvgrd(w2)%npt_u(2);y2(j)=wvgrd(w2)%org(2)+j*wvgrd(w2)%du(2);enddo
   do k=0,wvgrd(w2)%npt_u(3);z2(k)=wvgrd(w2)%org(3)+k*wvgrd(w2)%du(3);enddo
! now allocate z12,yz12,xyz12, for storing the results.
   allocate (z12(0:wvgrd(w2)%npt_u(3)))
   allocate (yz12(0:wvgrd(w2)%npt_u(2) , 0:wvgrd(w2)%npt_u(3)))
   allocate (xyz12(0:wvgrd(w2)%npt_u(1) , 0:wvgrd(w2)%npt_u(2) , 0:wvgrd(w2)%npt_u(3)))
! now calculate z.
   allocate (r1(0:npt1(3))); allocate (r2(0:npt2(3)))
   allocate (n12(0:npt2(3)))
! set up r2 vector.
   do k=0,wvgrd(w1)%npt_u(3); r1(k)=nint_z_FULL(wvfn(w1),z1(k)); enddo
   do k=0,wvgrd(w2)%npt_u(3); r2(k)=nint_z_FULL(wvfn(w2),z2(k)); enddo
   call sort_dist(npt1(3),npt2(3),r1,r2,n12)
   do k=0,wvgrd(w2)%npt_u(3)
    if (n12(k).eq.0) n12(k)=1; n12m=n12(k)-1
!    write(*,*) k,r2(k),n12m,n12(k), nint_z_FULL(wvfn(w2),z2(k)),&
!       nint_z_FULL(wvfn(w1),z1(n12m)), nint_z_FULL(wvfn(w1),z1(n12(k))) 
    call invert_nz(wvfn(w1),r2(k),r1(n12m),r1(n12(k)),z12(k))
   enddo
!   write(*,*) "Checking z: "
!   do k=0,wvgrd(w2)%npt_u(3)
!    write(*,*) k,z2(k),z12(k),nint_z_FULL(wvfn(w2),z2(k)),nint_z_FULL(wvfn(w1),z12(k))
!   enddo
!   write(*,*) "Checked z."
   deallocate(r1);deallocate(r2);deallocate(n12)
!   return
! y.
   allocate (r1(0:npt1(2))); allocate (r2(0:npt2(2)))
   allocate (n12(0:npt2(2)))
   do k=0,npt2(3) ! loop over z
    int2=nint_yz_FULL(wvfn(w2),100000._8,z2(k))
    int1=nint_yz_FULL(wvfn(w1),100000._8,z12(k))
    write(*,*) z2(k),z12(k),int2,int1
    if (int2.lt.0.0001) then  ! skip, fill in.
     write(*,*) "Skipping. " ;  do j=0,npt2(2); yz12(j,k)=y2(j); enddo
    else ! set up r1, r2 vectors.
     do j=0,npt1(2);r1(j)=nint_yz_FULL(wvfn(w1),y1(j),z12(k));enddo
     do j=0,npt2(2);r2(j)=nint_yz_FULL(wvfn(w2),y2(j),z2(k));enddo
     r2=r2*int1/int2
!     do j=0,npt2(2);write(*,*) j,r1(j)/int1,r2(j)/int1;enddo
     call sort_dist(npt1(2),npt2(2),r1,r2,n12)
     do j=0,npt2(2)
      if (n12(j).eq.0) n12(j)=1; n12m=n12(j)-1
!      write(*,*) j,"targ: ",r2(j),"lim: ",r1(n12m),r1(n12(j))
      call invert_nyz(wvfn(w1),z12(k),r2(j),y1(n12m),y1(n12(j)),yz12(j,k))
!      write(*,'(2I4,3F10.5)') j,k,nint_yz_FULL(wvfn(w2),y2(j),z2(k))/int2, nint_yz_FULL(wvfn(w1),yz12(j,k),z12(k))/int1, &
!          abs (nint_yz_FULL(wvfn(w2),y2(j),z2(k))/int2 - nint_yz_FULL(wvfn(w1),yz12(j,k),z12(k))/int1 )
     enddo
    endif
   enddo

   deallocate(r1);deallocate(r2);deallocate(n12)
! x.
   write(*,*) "doing x "
   allocate (r1(0:npt1(1))); allocate (r2(0:npt2(1)))
   allocate (n12(0:npt2(1)))
   do k=0,npt2(3); do j=0,npt2(2) ! loop over y, z
    int2=nint_xyz_FULL(wvfn(w2),100000._8,y2(j),z2(k))
    int1=nint_xyz_FULL(wvfn(w1),100000._8,yz12(j,k),z12(k))
!    write(*,*) z2(k),z12(k),int2,int1
    if (int2.lt.0.0001) then  ! skip, fill in.
     write(*,*) "Skipping ",j,k ;  do i=0,npt2(1); xyz12(i,j,k)=x2(i); enddo
    else ! set up r1, r2 vectors.
     write(*,*) "Calculating ",j,k
     do i=0,npt1(1);r1(i)=nint_xyz_FULL(wvfn(w1),x1(i),yz12(j,k),z12(k));enddo
     do i=0,npt2(1);r2(i)=nint_xyz_FULL(wvfn(w2),x2(i),y2(j),z2(k));enddo
     r2=r2*int1/int2
!     do j=0,npt2(2);write(*,*) j,r1(j)/int1,r2(j)/int1;enddo
     call sort_dist(npt1(1),npt2(1),r1,r2,n12)
     do i=0,npt2(1)
      if (n12(i).eq.0) n12(i)=1; n12m=n12(i)-1
      if (i.gt.0) xlow=max(x1(n12m),xyz12( (i-1),j,k))
!      write(*,*) j,"targ: ",r2(j),"lim: ",r1(n12m),r1(n12(j))
!      write(*,'(3I4,A8,F10.6,A6,2F9.5)') i,j,k," targ : ",r2(i), "lim: ",r1(n12m),r1(n12(i))
      call invert_nxyz(wvfn(w1),yz12(j,k),z12(k),r2(i),x1(n12m),x1(n12(i)),xyz12(i,j,k))
!      write(*,*) x2(i)," -> ",xyz12(i,j,k)
!      write(*,'(A25,F9.4,A8,F9.4)') "result: unscaled targ:",r2(i)/int1, " res: ", &
!       nint_xyz_FULL(wvfn(w1),xyz12(i,j,k),yz12(j,k),z12(k))/int1
!      write(*,'(2I4,3F10.5)') j,k,   nint_xyz_FULL(wvfn(w2),x2(i),y2(j),z2(k))/int2, & 
!         nint_xyz_FULL(wvfn(w1),xyz12(i,j,k),yz12(j,k),z12(k))/int1, &
!         abs(   nint_xyz_FULL(wvfn(w2),x2(i),y2(j),z2(k))/int2 -nint_xyz_FULL(wvfn(w1),xyz12(i,j,k),yz12(j,k),z12(k))/int1)
     enddo
    endif
   enddo;enddo

   if (allocated(wvfn(w2)%mos)) deallocate (wvfn(w2)%mos)
   allocate(wvfn(w2)%mos( wvfn(w2)%nA, 0:npt2(1),0:npt2(2),0:npt2(3)))
   allocate(mos(wvfn(w1)%nmos))
   do i=0,npt2(1);do j=0,npt2(2);do k=0,npt2(3)
    resvec=(/ xyz12(i,j,k),yz12(j,k),z12(k)/)
      call calc_MOs(wvfn(w1),resvec,mos)
!    wvfn(n)%dens(i,j,k)=calcdmat (wvfn(n),x,y,z) * 2.
!    wvfn(n)%cbos(i,j,k)=sqrt(wvfn(n)%dens(i,j,k) / (2. * wvfn(n)%nA))
      cbos=sqrt( calcdmat(wvfn(w1),xyz12(i,j,k),yz12(j,k),z12(k)) / wvfn(w1)%nA)
      mos=mos/cbos 
!      if (cbos.gt.0.0001) then
!       n2t=0.
!       do m=1,wvfn(w1)%nA
!        n2t=n2t+mos(m)**2
!       enddo
!      endif 
      cbos2=sqrt( calcdmat(wvfn(w2),x2(i),y2(j),z2(k)) / wvfn(w2)%nA)
      wvfn(w2)%mos(:,i,j,k)=mos(1:wvfn(w1)%nA)*cbos2
!      write(*,'(A5,3I3,2X,F6.2,2X,5F8.3)') "N2t: ",i,j,k,n2t,cbos,cbos2,wvfn(w2)%mos(1:3,i,j,k)
!      wvfn(w2)%mos=wvfn(w2)%mos * cbos
   enddo;enddo;enddo

   call cpu_time(time2)
   i0= nint(-(wvgrd(w1)%org(1)) / wvgrd(w1)%du(1) )
   j0= nint(-(wvgrd(w1)%org(2)) / wvgrd(w1)%du(2) )
   do m=1,wvfn(w1)%nA
    write(cf,'(A3,I1,A5)') "MOt",m,".cube"
    open(unit=15,file=cf,status="replace")
    write(15,*) " flurp";write(15,*) " "
    write(15,*) wvfn(w2)%nat,(wvgrd(w2)%org(k),k=1,3)
    write(15,*) 1+wvgrd(w2)%npt_u(1),wvgrd(w2)%du(1),0.,0.
    write(15,*) 1+wvgrd(w2)%npt_u(2),0.,wvgrd(w2)%du(2),0.
    write(15,*) 1+wvgrd(w2)%npt_u(3),0.,0.,wvgrd(w2)%du(3)
    do i=1,wvfn(w2)%nat;write(15,*) wvfn(w2)%zat(i),wvfn(w2)%zat(i)*1.,&
       (wvfn(w2)%xyz(i,k),k=1,3);enddo
    write(15,'(6F15.9)') ((( wvfn(w2)%mos(m,i,j,k),k=0,npt2(3)),j=0,npt2(2)),i=0,npt2(1))
    close(15)
    write(cf,'(A3,I1,A5)') "MOt",m,".line"
    open(unit=16,file=cf,status="replace")
     do k=0,npt2(3)
      if (m.eq.2) then
       write(16,*) (wvgrd(w2)%org(3)+k*wvgrd(w2)%du(3)), &
          0.99924*wvfn(w2)%mos(2,i0,j0,k)+0.039*wvfn(w2)%mos(3,i0,j0,k)
      elseif(m.eq.3) then
       write(16,*) (wvgrd(w2)%org(3)+k*wvgrd(w2)%du(3)), &
          0.99924*wvfn(w2)%mos(3,i0,j0,k)-0.039*wvfn(w2)%mos(2,i0,j0,k)
      else
       write(16,*) (wvgrd(w2)%org(3)+k*wvgrd(w2)%du(3)),wvfn(w2)%mos(m,i0,j0,k)
      endif
     enddo
    close(16)
   enddo

!   do k=0,wvgrd(w2)%npt_u(3)
!    z=wvgrd(w2)%org(3)+k*wvgrd(w2)%du(3);z1=z12(k)
!    nz1=nint_z_FULL (wvfn(w1),z1)
!    nz2=nint_z_FULL (wvfn(w2),z)
!    int2=nint_yz_FULL(wvfn(w2),100000._8,z)
!    int1=nint_yz_FULL(wvfn(w1),100000._8,z1)
!    do j=0,wvgrd(w2)%npt_u(2)
!     y=wvgrd(w2)%org(2)+j*wvgrd(w2)%du(2);y1=yz12(j,k)
!     ny1=nint_yz_FULL(wvfn(1),y1,z1)/int1
!     ny2=nint_yz_FULL(wvfn(2),y,z)/int2
!     write(*,'(2F9.4,X,4F9.4)') y,z,ny1,ny2,nz1,nz2
!   enddo;enddo
   write(*,*) "Time (not including cubefile write) : ",(time2-time1)
   return
  end subroutine N_trans2

  subroutine write_MOs(w1)
   implicit none
   integer,intent(in) :: w1
   integer :: i,j,k,l,m,npt(3),i0,j0
   character :: cf*15,cfl*20
   real(8) :: mos(1:wvfn(w1)%nmos),co(3)
   npt(:)=wvgrd(w1)%npt_u(:)
   if (allocated(wvfn(w1)%mos)) deallocate(wvfn(w1)%mos)
   allocate(wvfn(w1)%mos(1:wvfn(w1)%nA,0:npt(1),0:npt(2),0:npt(3)))
   
   do i=0,npt(1); co(1)=wvgrd(w1)%org(1)+wvgrd(w1)%du(1)*i
   do j=0,npt(2); co(2)=wvgrd(w1)%org(2)+wvgrd(w1)%du(2)*j
   do k=0,npt(3); co(3)=wvgrd(w1)%org(3)+wvgrd(w1)%du(3)*k
    call calc_MOs(wvfn(w1),co,mos)
    write(*,'(A8,3I3,8F8.4)') "modump:", i,j,k,(co(l),l=1,3),(mos(m),m=1,5)
    wvfn(w1)%mos(:,i,j,k)=mos(1:wvfn(w1)%nA) 
   enddo;enddo;enddo
   i0= nint(-(wvgrd(w1)%org(1)) / wvgrd(w1)%du(1) )
   j0= nint(-(wvgrd(w1)%org(2)) / wvgrd(w1)%du(2) )


   do m=1,wvfn(w1)%nA
    write(cf,'(A2,I1,A1,I1,A5)') "MO",w1,"_",m,".cube"
    open(unit=15,file=cf,status="replace")
    write(15,*) " flurp";write(15,*) " "
    write(15,*) wvfn(w1)%nat,(wvgrd(w1)%org(k),k=1,3)
    write(15,*) 1+wvgrd(w1)%npt_u(1),wvgrd(w1)%du(1),0.,0.
    write(15,*) 1+wvgrd(w1)%npt_u(2),0.,wvgrd(w1)%du(2),0.
    write(15,*) 1+wvgrd(w1)%npt_u(3),0.,0.,wvgrd(w1)%du(3)
    do i=1,wvfn(w1)%nat;write(15,*) wvfn(w1)%zat(i),wvfn(w1)%zat(i)*1.,&
       (wvfn(w1)%xyz(i,k),k=1,3);enddo
    write(15,'(6F15.9)') ((( wvfn(w1)%mos(m,i,j,k),k=0,npt(3)),j=0,npt(2)),i=0,npt(1))
    close(15)
    write(cfl,'(A2,I1,A1,I1,A5)') "MO",w1,"_",m,".line"
    open(unit=16,file=cfl,status="replace")
     do k=0,npt(3)
      write(16,*) (wvgrd(w1)%org(3)+k*wvgrd(w1)%du(3)),wvfn(w1)%mos(m,i0,j0,k)
     enddo
    close(16)
   enddo
   open(unit=15,file="dens_line",status="replace")
   open(unit=16,file="densf_line",status="replace")
   do k=0,npt(3)
    write(15,*) (wvgrd(w1)%org(3)+k*wvgrd(w1)%du(3)),wvfn(w1)%dens(i0,j0,k)
    write(16,*) nint_z_FULL(wvfn(w1),(wvgrd(w1)%org(3)+k*wvgrd(w1)%du(3))),wvfn(w1)%dens(i0,j0,k)
   enddo
   close(15)
   close(16)
   return
  end subroutine write_MOs

  subroutine sort_dist(np1,np2,r1,r2,n12)
   implicit none
   integer(4),intent(in) :: np1,np2
   real(8),intent(in) :: r1(0:np1),r2(0:np2)
   integer(4),intent(out):: n12(0:np2)
   integer :: i1,i2
   i1=0;i2=0

   do i2=0,np2
    if (r1(i1).ge.r2(i2)) then
     n12(i2)=i1
    else
     do while ((r1(i1).lt.r2(i2)).and.(i1.lt.np1))
      i1=i1+1
     enddo
     n12(i2)=i1
    endif
   enddo
   return
  end subroutine sort_dist


  subroutine N_transfer (w1,w2)
   implicit none
   integer,intent(in)  :: w1
   integer,intent(out) :: w2
   integer :: i,j,k,l,m,npts(3)
   real(8) :: x,y,z,xx,yy,zz,nx,ny,nz,nin(3),rout(3),intyz,intxyz
   real(8),allocatable :: mos(:),del(:,:)
   character :: cf*9
   npts(:)=wvgrd(w2)%npt_u(:)
   allocate(wvfn(w2)%mos( wvfn(w2)%nA, 0:npts(1),0:npts(2),0:npts(3)))
   allocate(mos(wvfn(w1)%nmos))
   allocate(del(wvfn(w1)%nA,wvfn(w1)%nA))
   del=0.
   do k=0,npts(3);z=wvgrd(w2)%org(3)+k*wvgrd(w2)%du(3)
    nz=nint_z_FULL(wvfn(w2),z)
    intyz=nint_yz_FULL(wvfn(w2),100000._8,z)
    write(*,*) k,nz
    do j=0,npts(2);y=wvgrd(w2)%org(2)+j*wvgrd(w2)%du(2)
     ny=nint_yz_FULL(wvfn(w2),y,z)/intyz
     write(*,*) "   ",j,ny
     intxyz=nint_xyz_FULL(wvfn(w2),100000._8,y,z)
     do i=0,npts(1);x=wvgrd(w2)%org(1)+i*wvgrd(w2)%du(1)
      nx=nint_xyz_FULL(wvfn(w2),x,y,z)/intxyz
!      write(*,*) "      ",i,nx
      nin=(/nx,ny,nz/)
      call N_lookup(wvfn(w1),nin,rout)
      call calc_MOs(wvfn(w1),rout,mos)
      mos=(mos/wvfn(w1)%cbos(i,j,k)) * wvfn(w2)%cbos(i,j,k)
      wvfn(w2)%mos(:,i,j,k)=mos(1:wvfn(w2)%nA)
      do m=1,wvfn(w1)%nA;do l=1,wvfn(w1)%nA 
       del(m,l)=del(m,l)+wvfn(w2)%mos(m,i,j,k)*wvfn(w2)%mos(l,i,j,k)
      enddo;enddo
   enddo;enddo;enddo
   do k=1,3;del=del*wvgrd(w2)%du(k);enddo
   do m=1,wvfn(w1)%nA
    write(*,'(6F10.5)') (del(m,l),l=1,5)
   enddo
   do m=1,wvfn(w1)%nA
    write(cf,'(A3,I1,A5)') "MO",m,".cube"
    open(unit=15,file=cf,status="replace")
    write(15,'(6F15.9)') ((( wvfn(w2)%mos(m,i,j,k),k=0,npts(3)),j=0,npts(2)),i=0,npts(1))
    close(15)
   enddo
   return
  end subroutine N_transfer

  subroutine calc_PW(n,kv)
   implicit none
   integer,intent(in) :: n,kv(3)
   integer :: i,j,k,l,m,npts(3),dimn
   character :: cfile*20,ck(3)*2
   real(8),allocatable :: moPW(:,:,:,:)
   integer,allocatable :: kPerm(:,:)
   real(8) :: tdens,x,y,z,moscr(0:4),nv(3),nvt(3),tau,nvyl,nvxl ! int to inf of y, x
   
   tau=4.*acos(0.)
   dimn=1;do i=1,3;if (kv(i).ne.0) dimn=dimn*2;enddo
   allocate(kPerm(dimn,3))
   call permute(dimn,kv,kPerm)
   do i=1,dimn;write(*,*) kPerm(i,:);enddo
   open(unit=21,file="ndump",status="replace") 
   npts(:)=wvgrd(n)%npt_u(:)
   allocate (moPW(dimn,0:npts(1),0:npts(2),0:npts(3)))
   do k=0,npts(3)
    z=wvgrd(n)%org(3)+k*wvgrd(n)%du(3)
    nv(3)=nint_z_FULL(wvfn(n),z)
    nvt(3)=nv(3)*tau
    nvyl=nint_yz_FULL(wvfn(n),100000._8,z)
    write(21,*) z,nv(3)
    do j=0,npts(2)
     y=wvgrd(n)%org(2)+j*wvgrd(n)%du(2)
     nv(2)=nint_yz_FULL(wvfn(n),y,z)/nvyl
     nvt(2)=nv(2)*tau
     write(21,*) y,z,nv(2)
     nvxl=nint_xyz_FULL(wvfn(n),100000._8,y,z)
     do i=0,npts(1)
      x=wvgrd(n)%org(1)+i*wvgrd(n)%du(1)
      nv(1)=nint_xyz_FULL(wvfn(n),x,y,z)/nvxl
      nvt(1)=nv(1)*tau
      write(21,'(7F7.2)') x,y,z,nv(1),nv(2),nv(3) ,wvfn(n)%cbos(i,j,k)
! n-space coords now in nv
! working pw mat moPW now needs 
      tdens=0.
      do l=1,dimn
       moscr(0)=sqrt(real(dimn))*sqrt(calcdmat(wvfn(n),x,y,z)/wvfn(n)%nA)  !*wvfn(n)%cbos(i,j,k)
       do m=1,3 ! x, y, z
        if (kPerm(l,m).eq.0)then; moscr(m)=1.
        elseif(kPerm(l,m).gt.0) then; moscr(m)=cos(kPerm(l,m)*nvt(m))
        else; moscr(m)=sin(kPerm(l,m)*nvt(m));endif
       enddo
       moscr(4)=moscr(0)*moscr(1)*moscr(2)*moscr(3)
       tdens=tdens+moscr(4)**2
       write(21,*) "modat : ", moscr(:)
       moPW(l,i,j,k)=moscr(4)
      enddo
      write(21,*) "PROP : ",calcdmat(wvfn(n),x,y,z)/tdens
     enddo
    enddo
   enddo

   close(21)
   write(*,*) "Planewave-based orbitals generated. "

   do l=1,dimn
    do m=1,3;
     if (kPerm(l,m).ge.0) then;  write(ck(m),'(A1,I1)') "+",kPerm(l,m);
     else; write(ck(m),'(I2)')kPerm(l,m); endif
     write(*,*) l,m,kPerm(l,m),ck(m)
    enddo
    cfile="MO_"//ck(1)//ck(2)//ck(3)
    write(*,*) kPerm(l,:)," file : ",cfile

    open(unit=15,file=cfile,status="replace")
    write(15,*) " flurp";write(15,*) " "
    write(15,*) wvfn(n)%nat,(wvgrd(n)%org(k),k=1,3)
    write(15,*) 1+wvgrd(n)%npt_u(1),wvgrd(n)%du(1),0.,0.
    write(15,*) 1+wvgrd(n)%npt_u(2),0.,wvgrd(n)%du(2),0.
    write(15,*) 1+wvgrd(n)%npt_u(3),0.,0.,wvgrd(n)%du(3)
    do i=1,wvfn(n)%nat;write(15,*) wvfn(n)%zat(i),wvfn(n)%zat(i)*1.,&
       (wvfn(n)%xyz(i,k),k=1,3);enddo
    write(15,'(6F15.9)') ((( moPW(l,i,j,k),k=0,npts(3)),j=0,npts(2)),i=0,npts(1))


    close(15)
   enddo
!   do m=1,wvfn(w1)%nA
!    write(cf,'(A3,I1,A5)') "MOt",m,".cube"
!    open(unit=15,file=cf,status="replace")
!    write(15,*) " flurp";write(15,*) " "
!    write(15,*) wvfn(w2)%nat,(wvgrd(w2)%org(k),k=1,3)
!    write(15,*) 1+wvgrd(w2)%npt_u(1),wvgrd(w2)%du(1),0.,0.
!    write(15,*) 1+wvgrd(w2)%npt_u(2),0.,wvgrd(w2)%du(2),0.
!    write(15,*) 1+wvgrd(w2)%npt_u(3),0.,0.,wvgrd(w2)%du(3)
!    do i=1,wvfn(w2)%nat;write(15,*) wvfn(w2)%zat(i),wvfn(w2)%zat(i)*1.,&
!       (wvfn(w2)%xyz(i,k),k=1,3);enddo
!    write(15,'(6F15.9)') ((( wvfn(w2)%mos(m,i,j,k),k=0,npt2(3)),j=0,npt2(2)),i=0,npt2(1))
!    close(15)
!    write(cf,'(A3,I1,A5)') "MOt",m,".line"
!    open(unit=16,file=cf,status="replace")
!     do k=0,npt2(3)
!      if (m.eq.2) then
!       write(16,*) (wvgrd(w2)%org(3)+k*wvgrd(w2)%du(3)), &
!          0.99924*wvfn(w2)%mos(2,i0,j0,k)+0.039*wvfn(w2)%mos(3,i0,j0,k)
!      elseif(m.eq.3) then
!       write(16,*) (wvgrd(w2)%org(3)+k*wvgrd(w2)%du(3)), &
!          0.99924*wvfn(w2)%mos(3,i0,j0,k)-0.039*wvfn(w2)%mos(2,i0,j0,k)
!      else
!       write(16,*) (wvgrd(w2)%org(3)+k*wvgrd(w2)%du(3)),wvfn(w2)%mos(m,i0,j0,k)
!      endif
!!     enddo
!    close(16)
!   enddo
!
   return
  end subroutine calc_PW

subroutine permute(dm,nv,kP)
implicit none
integer, intent(in) :: dm,nv(3)
integer,intent(out) :: kP(dm,3)
integer :: i,ii,iii,j,k,u1,u2

if (dm.eq.1) then;kP(1,:)=nv(:);return
elseif (dm.eq.8) then
 do i=1,8
  kP(i,:)=nv(:)
  ii=i
  if (ii.gt.4) then;
    kP(i,3)=kP(i,3)*(-1);  ii=ii-4 ; endif
  if (ii.gt.2) then;
    kP(i,2)=kP(i,2)*(-1); ii=ii-2 ; endif
  if (ii.gt.1) then;
    kP(i,1)=kP(i,1)*(-1); endif
 enddo
elseif (dm.eq.2) then
 do i=1,3;if (nv(i).ne.0) u1=i;enddo
 kP(1,:)=nv(:)
 kP(2,:)=nv(:)
 kP(2,u1)=kP(2,u1)*(-1)
elseif (dm.eq.4) then
 if (nv(1).eq.0) then;u1=2;u2=3
 elseif(nv(2).eq.0) then;u1=1;u2=3
 else; u1=1;u2=2;endif
 do i=1,4
  kP(i,:)=nv(:)
  ii=i
  if (ii.gt.2) then;
   kP(i,u2)=kP(i,u2)*(-1); ii=ii-2;endif
  if (ii.gt.1) then;
   kP(i,u1)=kP(i,u1)*(-1); endif
 enddo

endif

return
end


  subroutine calc_dens(n)
   implicit none
   integer,intent(in) :: n
   integer :: i,j,k,n1,n2,n3
   real(8) :: x,y,z
   n1=wvgrd(n)%npt_u(1)
   n2=wvgrd(n)%npt_u(2)
   n3=wvgrd(n)%npt_u(3)
   allocate(wvfn(n)%dens(0:n1,0:n2,0:n3))
   allocate(wvfn(n)%cbos(0:n1,0:n2,0:n3))
   do i=0,n1;x=wvgrd(n)%org(1) + i*wvgrd(n)%du(1)
   do j=0,n2;x=wvgrd(n)%org(2) + j*wvgrd(n)%du(2)
   do k=0,n3;x=wvgrd(n)%org(3) + k*wvgrd(n)%du(3)
    wvfn(n)%dens(i,j,k)=calcdmat (wvfn(n),x,y,z) * 2.
    wvfn(n)%cbos(i,j,k)=sqrt(wvfn(n)%dens(i,j,k) / (2. * wvfn(n)%nA))
   enddo;enddo;enddo
   return 
  end subroutine calc_dens

  subroutine disp_wv(i)
   implicit none
   integer,intent(in) :: i
   
   write(*,*) 
   if (awvfn(i)) then
    write(*,*) "SLOT ",i, " : ",wvfn(i)%nomen
    write(*,*) wvfn(i)%nA," alpha orbitals.", wvfn(i)%nbas," basis functions.", &
               wvfn(i)%nprim," primitives."
    write(*,*) "Grid : ", wvgrd(i)%npt_u(1)," x ",wvgrd(i)%npt_u(2)," x ",wvgrd(i)%npt_u(3)," = ", &
     wvgrd(i)%npts," points. "  
   else
    write(*,*) "SLOT ",i," unallocated."
   endif
   return
  end subroutine disp_wv

end module OPTIONS
