module misc

 contains

  real(8) function calc_vecX(c,x)
   implicit none
   real(8),intent(in) :: c(0:10),x
   integer :: i
   calc_vecX=0.
   do i=0,10;if (c(i).eq.0.) cycle
    calc_vecX=calc_vecX+c(i)*(x**i)
   enddo
   return
  end function calc_vecX

  subroutine EXPANDX(r1,r2,p1,p2,c)
   real(8),intent(in) ::r1,r2
   real(8),intent(out)::c(0:10)
   integer,intent(in)::p1,p2
   integer::i,j,k,ptot
   real(8) :: c1(0:p1),c2(0:p2) !,x,fx1,fx2
   c1=0.;c2=0.
   do i=0,p1;c1(p1-i)=BIN(p1,i)*r1**i;enddo
   do j=0,p2;c2(p2-j)=BIN(p2,j)*r2**j;enddo
!   write(*,*) "C1: sum of "
!   do i=0,p1;write(*,*) c1(i)," * x ^",i;enddo
!   write(*,*) "C2: sum of "
!   do i=0,p2;write(*,*) c2(i)," * x ^",i;enddo

   c=0.
   do i=0,p1;do j=0,p2;ptot=i+j
    c(ptot)=c(ptot)+c1(i)*c2(j)
   enddo;enddo
!   write(*,*) "Ctotal: sum of "
!   do i=0,p1+p2;write(*,*) c(i)," * x ^",i;enddo

!   do i=-100,100;x=i/10.
!    fx1=(x+r1)**p1+(x+r2)**p2
!   enddo
   return
  end subroutine EXPANDX

  integer function BIN(n,k)
   integer,intent(in)::n,k
   integer :: i,i_n,i_d
   if (k.eq.0) then;BIN=1;return;endif
   if ( (n.lt.0).or.(k.lt.0).or.(n.lt.k)) then
    write(*,*) "Binomial ",n," over ",k," not possible. End program.";stop
   endif
   i_n=1;i_d=1
   do i=1,k
    i_n=i_n*(n+1-i);
    i_d=i_d*i
   enddo
   BIN=i_n/i_d
   return
  end function BIN
end module misc
