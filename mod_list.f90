module LIST

 implicit none

 type NODE 
!  class(*),pointer   :: self  => null()
  type(NODE),pointer :: next  => null()
  real(8)            :: rval
  real(8)            :: nval
 end type NODE

 type LLIST
  integer(4) :: lsize = 0
  type(NODE),pointer :: first => null()
  type(NODE),pointer :: last  => null()
 end type LLIST

 contains
  subroutine append (nd,rv,nv)
   implicit none
   type(NODE),pointer,intent(inout) :: nd
   real(8),intent(in) :: rv,nv
   type(NODE),pointer :: newnode
   type(NODE),pointer :: nextnode

!   nextnode=nd%next
   allocate(newnode)
   newnode%next=>nd%next
   nd%next=>newnode
   newnode%rval=rv
   newnode%nval=nv
   return
  end subroutine append

end module LIST

