 program newmain
 use OPTIONS
 use PRIMS
 implicit none
 character :: cfile*25

 type( wavefn) :: wv1
 integer :: i,j,k,l
 real(8) :: x,y,z,x1,t1,t2
 real(8) :: nxyz(0:300,0:300,0:300)
 real(8) :: nyz(0:300,0:300)
 real(8) :: nz(0:300),an_xyz(0:16,0:16,0:16),an_yz(0:16,0:16),an_z(0:16), nnum(0:300,0:300,0:300,3)
 real(8) :: d,cube(0:300,0:300,0:300),cbos(0:300,0:300,0:300)
 real(8) :: dens(0:300,0:300,0:300)
 real(8) :: cn(0:300,0:300,0:300,3),fac
 real(8) :: co1(3),co2(3),co2a(3),r2,r2max,n_t1,n_t2,z1,z2,xout,yout,zout,ni,intyz,intxyz,nintxyz,diff
 integer :: npts,hnpts,inc
 real(8) :: du,sm,umin,jkl(3)
 real(8),allocatable :: mos(:)
 logical :: acont
 character :: yn*1,cnewname*20,cf(10)*20
 ! function declaration:
 real(8) :: fitdens

! new version: just call subroutines that do everything
 call INIT()
 call MENU()
 stop
! old version: ....
 cf(1)="MO1.cube"
 cf(2)="MO2.cube"
 cf(3)="MO3.cube"
 cf(4)="MO4.cube"
 cf(5)="MO5.cube"
 cf(6:)="other.cube"

! read formatted checkpoint file into wv1
 call getarg(1,cfile)
 call readformchk (cfile,wv1)
 call PRIMDUMP(wv1)

! set up a grid... later this will need to take additional input to allow
! user specification of grid details
 npts=100;hnpts=50;du=0.1_8
 umin=-hnpts*du
 write(*,*) "nA : ",wv1%nA
 open (unit=77,file="zplot",status="replace")
 do k=0,npts;z=(k-hnpts)*du
  write(77,*) z,nint_z_FULL(wv1,z)
 enddo
 close(77)
 open (unit=77,file="yplot",status="replace")
 do j=0,npts;y=(j-hnpts)*du
  write(77,*) y,nint_yz_FULL(wv1,y,0._8)/nint_yz_FULL(wv1,10000._8,0._8)
 enddo
 close(77)
 open (unit=77,file="xplot",status="replace")
 do i=0,npts;x=(i-hnpts)*du
  write(77,*) x,nint_xyz_FULL(wv1,x,0._8,0._8)/nint_xyz_FULL(wv1,10000._8,0._8,0._8)
 enddo
 close(77)

! calculate bosonic wavefunction
 do i=0,npts;do j=0,npts;do k=0,npts
  dens(i,j,k)=2.*calcdmat(wv1,(i-hnpts)*du,(j-hnpts)*du,(k-hnpts)*du)
  cbos(i,j,k)=sqrt(calcdmat(wv1,(i-hnpts)*du,(j-hnpts)*du,(k-hnpts)*du)/wv1%nA)
 enddo;enddo;enddo
 call write2cube("density.cube",dens,npts)
 write(*,*) "Density written "
 allocate(mos(wv1%nmos))

 cube=0.
 dens=0.
write(*,*) "alpha electrons : ",wv1%nA
do l=1,wv1%nA
 do i=0,npts;x=(i-hnpts)*du
 do j=0,npts;y=(j-hnpts)*du
 do k=0,npts;z=(k-hnpts)*du
  co1=(/x,y,z/)
  call calc_MOs(wv1,co1,mos)
  cube(i,j,k)=mos(l)
!  if (cbos(i,j,k).gt.0.000000000001) cube(i,j,k)=mos(l) /cbos(i,j,k)
 enddo;enddo;enddo
 write(*,*) "writing ",cf(l)
 call write2cube(cf(l),cube,npts)
 dens=dens+cube**2
enddo 
dens=dens*2
write(*,*) "MOs written; writing density from MOs"
call write2cube("d_MOs.cube",dens,npts)



! call cpu_time(t1)
! do i=0,100;x=i*0.01
!!  write(*,*) "searching for nz ",x
!  x1=inv_nz(wv1,x)
!  write(*,*) x1,x,nint_z(wv1,x1)
! enddo
! call cpu_time(t2)
! write(*,*) t1,t2,(t2-t1)/101.
! do l=1,1
! sm=0.
! do k=0,npts;z=(k-hnpts)*du
! do j=0,npts;y=(j-hnpts)*du
! do i=0,npts;x=(i-hnpts)*du
!  sm=sm+(du*du*du)*calcdmat(wv1,x,y,z)
! enddo;enddo;enddo
! write(*,*) "Total density integration for MO ",l, " : ",sm
! enddo

!! check:
! do k=0,npts;z=(k-hnpts)*du;do j=0,npts;y=(j-hnpts)*du;
!  do i=1,npts;x=(i-hnpts)*du
!   n_t1=nint_xyz(wv1,x,y,z)
!   n_t2=nint_xyz_FULL(wv1,x,y,z)
!  if (abs(n_t1-n_t2).gt.0.00000001) then
!   write(*,'(6F12.8)') x,y,z,n_t1,n_t2,10000.*(n_t1-n_t2)
!  endif
! enddo;enddo;enddo
 i=16
 call INVERT_N(wv1,i) 
 write(*,*) nint_z_FULL(wv1,20._8)
 wv1%nel=1
 write(*,*) nint_z_FULL(wv1,20._8)



!! loop over all points (indices i,j,k; cart.co's x,y,z)
!! calculate nxyz numerically
! do k=0,npts;z=(k-hnpts)*du; do j=0,npts;y=(j-hnpts)*du; 
!  nxyz(0,j,k)=calcdmat(wv1,umin,y,z)*du
!  do i=1,npts;x=(i-hnpts)*du
!   d=fitdens(x,y,z)*du
!   !d=calcdmat(wv1,x,y,z)*du
!   nxyz(i,j,k)=nxyz(i-1,j,k)+d
!  enddo
!!!  if (nxyz(npts,j,k).gt.0._8) nxyz(:,j,k)=nxyz(:,j,k)/nxyz(npts,j,k)
! enddo;enddo
! 
!! calculate nyz numerically
! do k=0,npts;z=(k-hnpts)*du
!  nyz(0,k)=nxyz(npts,0,k)
!  do j=1,npts;y=(j-hnpts)*du
!   nyz(j,k)=nyz(j-1,k)+nxyz(npts,j,k)*du
!  enddo
!!!  if (nyz(npts,k).gt.0._8) nyz(:,k)=nyz(:,k)/nyz(npts,k)
! enddo
!! calculate nz numerically
! nz(0)=nyz(npts,0)
! do k=1,npts;z=(k-hnpts)*du
!  nz(k)=nz(k-1)+nyz(npts,k)*du
! enddo
! 
! ! dump nz, also normalise
! if (nz(npts).gt.0._8) then
!  write(*,*) "Total dens ",nz(npts)
!  do k=0,npts;write(*,*)  (k-hnpts)*du,nz(k)/5.,nint_z(wv1,(k-hnpts)*du);enddo
!!  nz(:)=nz(:)/nz(npts)
! endif
! 
!! !dump nyz, nxyz
!! write(*,*) "yz check"
!! do j=0,npts
!!  write(*,*) (j-hnpts)*du,nyz(j,70),nint_yz(wv1,(j-hnpts)*du,(70-hnpts)*du)
!! enddo
!! write(*,*) "xyz check"
!! do i=0,npts
!!  write(*,*) (i-hnpts)*du,nxyz(i,70,80),nint_xyz(wv1,(i-hnpts)*du,(70-hnpts)*du,(80-hnpts)*du)
!! enddo
 
! ! normalise nxyz
! do k=0,npts;do j=0,npts;
!  if (nxyz(npts,j,k).gt.0.) then 
!   do i=0,npts
!    nxyz(i,j,k)=nxyz(i,j,k)/nxyz(npts,j,k)
!   enddo
!  endif
! enddo;enddo
! ! normalise nyz
! write(*,*) "yz check"
! do k=0,npts;if (nyz(npts,k).gt.0) then
! do j=0,npts
!!  nyz(j,k)=nyz(j,k)/nyz(npts,k)
! enddo;endif;enddo
! ! normalise nz (again?)
! do k=0,npts
!  nz(k)=nz(k)/nz(npts)
! enddo
! do i=0,npts;do j=0,npts;do k=0,npts
!  nnum(i,j,k,1)=nxyz(i,j,k)
!  nnum(i,j,k,2)=nyz(j,k)
!  nnum(i,j,k,3)=nz(k)
! enddo;enddo;enddo

! now we have a normalised numerical integration over the grid
! (low-quality numerical integration!)

! do k=0,npts; do j=0,npts
! do i=0,npts;nxyz(i,j,k)=nxyz(i,j,k)/nyz(j,k)
! enddo;nyz(j,k)=nyz(j,k)/nz(k)
! enddo;nz(k)=nz(k)/nz(npts)
! enddo


 ! calc *analytical* n 
 do k=0,npts;co1(3)=(k-hnpts)*du;
 do j=0,npts;co1(2)=(j-hnpts)*du;
 do i=0,npts;co1(1)=(i-hnpts)*du;
  cn(i,j,k,:)=get_n(wv1,co1) 
 enddo;enddo;enddo 

 
! dump density, n, to file
!call write2cube("num_x.cube",nnum(:,:,:,1),npts)
!call write2cube("num_y.cube",nnum(:,:,:,2),npts)
!call write2cube("num_z.cube",nnum(:,:,:,3),npts)
!call write2cube("an_x.cube",cn(:,:,:,1),npts)
!call write2cube("an_y.cube",cn(:,:,:,2),npts)
!call write2cube("an_z.cube",cn(:,:,:,3),npts)

! loop: get i,j,k from user, output psi_ijk 
 acont=.true.
 do while (acont)
  write(*,*) "enter i j k"
  read(*,*) (jkl(k),k=1,3)
! for each point....
  do i=0,npts;do j=0,npts;do k=0,npts
   fac=1.  ! factor
 
!   ! use numerical
!    if (jkl(1).ge.-0.0001) then; fac=fac*cos(jkl(1)*2.*PI*nxyz(i,j,k))
!    else;                        fac=fac*sin(jkl(1)*2.*PI*nxyz(i,j,k))
!    endif   
!    if (jkl(2).ge.-0.0001) then; fac=fac*cos(jkl(2)*2.*PI*nyz(j,k))
!    else;                        fac=fac*sin(jkl(2)*2.*PI*nyz(j,k))
!    endif
!    if (jkl(3).ge.-0.0001) then; fac=fac*cos(jkl(3)*2.*PI*nz(k))
!    else;                        fac=fac*sin(jkl(3)*2.*PI*nz(k))
!    endif
        
   ! use analytical
   do l=1,3  
    if (jkl(l).ge.-0.0001) then; fac=fac*cos(jkl(l)*2.*PI*cn(i,j,k,l))
    else;                        fac=fac*sin(jkl(l)*2.*PI*cn(i,j,k,l))
    endif;

! some dumping stuff
!    if ((l.eq.3).and.(i.eq.hnpts).and.(j.eq.hnpts)) then;
!     write(*,*) i,j,k,cn(i,j,k,:),fac
!    endif
   enddo

   cube(i,j,k)=cbos(i,j,k)*fac
  enddo;enddo;enddo ! loop over points
  write(*,*) "Enter new filename";read(*,*) cnewname
  call write2cube(cnewname,cube,npts)
 enddo
 stop
 end

 subroutine write2cube(cubename,dat,npts)
 character(len=*),intent(in) :: cubename
 real(8),intent(in) :: dat(0:300,0:300,0:300)
 integer,intent(in) :: npts
 integer :: i,j,k
 
 open(unit=89,file=cubename,status="replace")
 write(89,'(6F10.5)') (((dat(i,j,k),k=0,npts),j=0,npts),i=0,npts)
 close(89)
 return;end subroutine write2cube

 real(8) function fitdens(x,y,z)
 real(8),intent(in):: x,y,z
 real(8) :: r2
 r2=x**2+y**2+z**2
 fitdens=137.685*exp(-271.248*r2)
 fitdens=fitdens+54.1575*exp(-58.5714*r2)
 fitdens=fitdens+1.99342*exp(-2.02593*r2) 
 return;end function fitdens
